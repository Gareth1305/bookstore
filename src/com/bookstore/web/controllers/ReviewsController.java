package com.bookstore.web.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bookstore.web.dao.Book;
import com.bookstore.web.dao.Customer;
import com.bookstore.web.dao.Review;
import com.bookstore.web.service.BooksService;
import com.bookstore.web.service.CustomersService;
import com.bookstore.web.service.ReviewsService;

@Controller
public class ReviewsController {
	
	private ReviewsService reviewsService;

	@Autowired
	public void setReviewsService(ReviewsService reviewsService) {
		this.reviewsService = reviewsService;
	}
	
	private CustomersService customersService;
	
	@Autowired
	public void setCustomersService(CustomersService customersService) {
		this.customersService = customersService;
	}
	
	private BooksService booksService;
	
	@Autowired
	public void setAdministratorsService(BooksService booksService) {
		this.booksService = booksService;
	}
	
	@RequestMapping(value = "/api/createReview", method = RequestMethod.POST)
	@ResponseBody
	public void receiveReviewInfo(@RequestBody String reviewJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(reviewJson, new TypeReference<HashMap<String, String>>() {
			});
			createReview(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createReview(Map<String, String> map) {
		String username = (String) map.get("username");
		String rating = (String) map.get("rating");
		int formattedRating = Integer.parseInt(rating);
		String comment = (String) map.get("comment");
		String bookID = (String) map.get("bookID");
		int formattedBookID = Integer.parseInt(bookID);

		Book book = booksService.getBookByID(formattedBookID);
		Customer customer = customersService.getCustomerByUsername(username);
		Review review = new Review(formattedRating, comment, book, customer);
		reviewsService.saveOrUpdateReview(review);
		
		if(book.getReviews().size() == 0) {
			List<Review> reviews = new ArrayList<Review>();
			reviews.add(review);
			book.setReviews(reviews);
			booksService.saveOrUpdate(book);
		} else {
			List<Review> reviews = book.getReviews();
			reviews.add(review);
			book.setReviews(reviews);
			booksService.saveOrUpdate(book);
		}
		
	}
	
	
	
	@RequestMapping(value = "/api/getAllReviews", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Review> getAllReviews() {
		return reviewsService.getAllReviews();
	}

}
