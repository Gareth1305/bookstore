package com.bookstore.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bookstore.web.dao.Book;
import com.bookstore.web.dao.PurchaseHistory;
import com.bookstore.web.service.PurchaseHistorysService;

@Controller
public class PurchaseHistorysController {

	private PurchaseHistorysService purchaseHistorysService;

	@Autowired
	public void setPurchaseHistorysService(PurchaseHistorysService purchaseHistorysService) {
		this.purchaseHistorysService = purchaseHistorysService;
	}
	
	@RequestMapping(value = "/api/getAllPurchaseHistorys", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<PurchaseHistory> getAllPurchaseHistorys() {
		return purchaseHistorysService.getAllPurchaseHistorys();
	}

}
