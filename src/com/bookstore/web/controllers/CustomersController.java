package com.bookstore.web.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bookstore.web.dao.Book;
import com.bookstore.web.dao.Cart;
import com.bookstore.web.dao.Customer;
import com.bookstore.web.dao.PurchaseHistory;
import com.bookstore.web.dao.Review;
import com.bookstore.web.service.CartsService;
import com.bookstore.web.service.CustomersService;

@Controller
public class CustomersController {

	private CustomersService customersService;
	
	@Autowired
	public void setCustomersService(CustomersService customersService) {
		this.customersService = customersService;
	}
	
	private CartsService cartsService;
	
	@Autowired
	public void setCustomersService(CartsService cartsService) {
		this.cartsService = cartsService;
	}

	@RequestMapping("/newaccount")
	public String showNewAccount() {
		return "newaccount";
	}
	
	@RequestMapping(value = "/api/createCustomer", method = RequestMethod.POST)
	@ResponseBody
	public void registerResident(@RequestBody String residentJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(residentJson, new TypeReference<HashMap<String, String>>() {
			});
			createCustomer(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCustomer(Map<String, String> map) {
		String username = (String) map.get("customername");
		String password = (String) map.get("password");
		String email = (String) map.get("email");
		boolean enabled = true;
		String authority = "ROLE_CUSTOMER";
		
		String address = (String) map.get("address");
		String city = (String) map.get("city");
		String county = (String) map.get("county");
		String country = (String) map.get("country");
		Cart cart = new Cart();
		List<PurchaseHistory> ph = new ArrayList<>();
		Customer customer = new Customer(username, password, enabled, authority, email, address, 
				city, county, country, cart, ph);
		cartsService.saveOrUpdate(cart);
		customersService.saveOrUpdate(customer);
	}
	
	@RequestMapping(value = "/api/getAllCustomers", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Customer> getAllCustomers() {
		return customersService.getCustomers();
	}

	@RequestMapping(value = "/api/getCustomerByUsername/{username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Customer getCustomerByUsername(@PathVariable String username) {
		return customersService.getCustomerByUsername(username);
	}
	
	@RequestMapping(value = "/api/getHistoryByUsername/{username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<PurchaseHistory> getHistoryByUsername(@PathVariable String username) {
		return customersService.getHistoryByUsername(username);
	}
}
