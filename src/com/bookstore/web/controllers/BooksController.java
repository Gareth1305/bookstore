package com.bookstore.web.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bookstore.web.dao.Administrator;
import com.bookstore.web.dao.Book;
import com.bookstore.web.dao.Review;
import com.bookstore.web.patterns.CareTaker;
import com.bookstore.web.patterns.Originator;
import com.bookstore.web.service.AdministratorsService;
import com.bookstore.web.service.BooksService;
import com.bookstore.web.service.ReviewsService;

@Controller
public class BooksController {

	private BooksService booksService;
	
	@Autowired
	public void setAdministratorsService(BooksService booksService) {
		this.booksService = booksService;
	}
	
	private ReviewsService reviewsService;
	
	@Autowired
	public void setAdministratorsService(ReviewsService reviewsService) {
		this.reviewsService = reviewsService;
	}	
	
	@RequestMapping(value = "/api/getAllBooks", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Book> getAllBooks() {
		return booksService.getAllBooks();
	}
	
	@RequestMapping("/createbook")
	public String createbooks() {
		return "createbook";
	}
	
	@RequestMapping("/booksadminview")
	public String booksAdminView() {
		return "booksadminview";
	}
	
	@RequestMapping("/books")
	public String books() {
		return "books";
	}
	
	@RequestMapping(value = "/api/updateBook", method = RequestMethod.POST)
	@ResponseBody
	public void registerResident(@RequestBody String bookJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(bookJson, new TypeReference<HashMap<String, String>>() {
			});
			updateBook(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void updateBook(Map<String, String> map) {
		String idToConvert = (String) map.get("id");
		int id = Integer.parseInt(idToConvert);
		String booktitle = (String) map.get("bookTitle");
		String bookauthor = (String) map.get("bookAuthor");
		String bookgenre = (String) map.get("bookGenre");
		String bookPriceToConvert = (String) map.get("bookPrice");
		double bookprice = Double.parseDouble(bookPriceToConvert);
		String bookQuantityToConvert = (String) map.get("bookQuantity");
		int bookquantity = Integer.parseInt(bookQuantityToConvert);
		String confirmVal = (String) map.get("confirmVal");
		boolean value = false;
		if(confirmVal.equals("true")) {
			value = true;
		}
		
		CareTaker careTaker = new CareTaker();
		Originator originator = new Originator(booktitle, bookauthor, bookprice, bookgenre, bookquantity, careTaker);
		
		originator.setTitle(booktitle);
		originator.setAuthor(bookauthor);
		originator.setGenre(bookgenre);
		originator.setPrice(bookprice);
		originator.setNoInStock(bookquantity);
		originator.createSavepoint("SaveBookInfo");
		
		if(value == false) {
			originator.undoAll();
		} else {
			Book book = booksService.getBookByID(id);
			book.setBookTitle(originator.getTitle());
			book.setBookAuthor(originator.getAuthor());
			book.setBookGenre(originator.getGenre());
			book.setBookPrice(originator.getPrice());
			book.setBookQuantity(originator.getNoInStock());
			booksService.saveOrUpdate(book);
		}
	}
	
	@RequestMapping(value = "/api/createBook", method = RequestMethod.POST)
	@ResponseBody
	public void createBook(@RequestBody String bookJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(bookJson, new TypeReference<HashMap<String, String>>() {
			});
			createBook(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createBook(Map<String, String> map) {
		String booktitle = (String) map.get("booktitle");
		String bookauthor = (String) map.get("bookauthor");
		String bookgenre = (String) map.get("bookgenre");
		String bookPriceToConvert = (String) map.get("bookprice");
		double bookprice = Double.parseDouble(bookPriceToConvert);
		String bookimagetitle = (String) map.get("bookimagetitle");
		String bookQuantityToConvert = (String) map.get("bookquantity");
		int bookquantity = Integer.parseInt(bookQuantityToConvert);
		
		List<Review> reviews = new ArrayList<Review>();
//		reviewsService.saveOrUpdate(reviews);
		Book book = new Book(booktitle, bookauthor, bookgenre, bookprice, bookimagetitle, 
				bookquantity, reviews);
		booksService.saveOrUpdate(book);
	}
	
}
