package com.bookstore.web.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bookstore.web.dao.Book;
import com.bookstore.web.dao.Cart;
import com.bookstore.web.dao.Customer;
import com.bookstore.web.dao.LineItem;
import com.bookstore.web.dao.PHLineItem;
import com.bookstore.web.dao.PurchaseHistory;
import com.bookstore.web.patterns.CalculateDiscountStrategy;
import com.bookstore.web.patterns.LargeDiscountStrategy;
import com.bookstore.web.patterns.SmallDiscountStrategy;
import com.bookstore.web.service.BooksService;
import com.bookstore.web.service.CartsService;
import com.bookstore.web.service.CustomersService;
import com.bookstore.web.service.LineItemsService;
import com.bookstore.web.service.PHLineItemsService;
import com.bookstore.web.service.PurchaseHistorysService;

@Controller
public class CartsController {

	private PurchaseHistorysService purchaseHistorysService;

	@Autowired
	public void setPurchaseHistorysService(PurchaseHistorysService purchaseHistorysService) {
		this.purchaseHistorysService = purchaseHistorysService;
	}
	
	private PHLineItemsService phLineItemsService;

	@Autowired
	public void setPHLineItemsService(PHLineItemsService phLineItemsService) {
		this.phLineItemsService = phLineItemsService;
	}
	
	private CartsService cartsService;

	@Autowired
	public void setCartsService(CartsService cartsService) {
		this.cartsService = cartsService;
	}
	
	private LineItemsService lineItemsService;
	
	@Autowired
	public void setLineItemsService(LineItemsService lineItemsService) {
		this.lineItemsService = lineItemsService;
	}
	
	private CustomersService customersService;
	
	@Autowired
	public void setCustomersService(CustomersService customersService) {
		this.customersService = customersService;
	}
	
	private BooksService booksService;
	
	@Autowired
	public void setAdministratorsService(BooksService booksService) {
		this.booksService = booksService;
	}
	
	
	@RequestMapping(value = "/api/getShoppingCart/{username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Cart getShoppingCart(@PathVariable String username) {
		return customersService.getShoppingCart(username);
	}
	
	
	@RequestMapping(value = "/api/clearCart", method = RequestMethod.POST)
	@ResponseBody
	public void clearCart(@RequestBody String username) {
		username = username.replace("\"", "");
		Customer customer = customersService.getCustomerByUsername(username);
		Cart cart = customer.getCart();
		
		List<LineItem> itemsToUpdateBookNumbers = cart.getItems();
		for(int i = 0; i < itemsToUpdateBookNumbers.size(); i++) {
			Book book = booksService.getBookByID(itemsToUpdateBookNumbers.get(i).getBook().getId());
			book.setBookQuantity(book.getBookQuantity()+itemsToUpdateBookNumbers.get(i).getQuantity());
			booksService.saveOrUpdate(book);
		}
		
		double newPrice = 0.00;
		List<LineItem> items = new ArrayList<>();

		cart.setOrderCost(newPrice);
		cart.setItems(items);
		cartsService.saveOrUpdate(cart);
	}
	
	
	@RequestMapping(value = "/api/plusButtonPressed", method = RequestMethod.POST)
	@ResponseBody
	public void plusButtonPressed(@RequestBody String changesInJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(changesInJson, new TypeReference<HashMap<String, String>>() {
			});
			savePlusChanges(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void savePlusChanges(Map<String, String> map) {
		String username = (String) map.get("username");
		String idToConvert = (String) map.get("id");
		int id = Integer.parseInt(idToConvert);
		String bookpriceToConvert= (String) map.get("bookprice");
		double bookprice = Double.parseDouble(bookpriceToConvert);
		
		Customer customer = customersService.getCustomerByUsername(username);
		Cart cart = customer.getCart();		
		Book book = booksService.getBookByID(id);
		
		int quantityBooks = book.getBookQuantity();
		int remainingBooks = quantityBooks - 1;
		book.setBookQuantity(remainingBooks);
		booksService.saveOrUpdate(book);
		
		List<LineItem> items = cart.getItems();
		for(LineItem item: items) {
			if(item.getBook().getId() == id) {
				item.setQuantity(item.getQuantity()+1);
				item.setBook(book);
				lineItemsService.saveOrUpdate(item);
			}
		}
		
		double price = cart.getOrderCost();
		double newPrice = price + bookprice;
		cart.setOrderCost(newPrice);
		cartsService.saveOrUpdate(cart);
	
		customer.setCart(cart);
		customersService.saveOrUpdateCustomerWithoutUpdatingPassword(customer);
	}


	@RequestMapping(value = "/api/saveCart", method = RequestMethod.POST)
	@ResponseBody
	public void registerResident(@RequestBody String cartJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(cartJson, new TypeReference<HashMap<String, String>>() {
			});
			saveToCart(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveToCart(Map<String, String> map) {
		String username = (String) map.get("username");
		
		String priceToConvert = map.get("price");
		double price = Double.parseDouble(priceToConvert);
		
		String bookIdToConvert = map.get("bookId");
		int bookId = Integer.parseInt(bookIdToConvert);
		
		String bookQuantityToConvert = map.get("bookQuantity");
		int quantity = Integer.parseInt(bookQuantityToConvert);
		
		Book book = booksService.getBookByID(bookId);
		int quantityBooks = book.getBookQuantity();
		int remainingBooks = quantityBooks - quantity;
		book.setBookQuantity(remainingBooks);
		booksService.saveOrUpdate(book);
		
		Customer customer = customersService.getCustomerByUsername(username);
		Cart cart = customer.getCart();
		
		LineItem item = new LineItem(quantity, book);
		lineItemsService.saveOrUpdate(item);
		
		cart.setOrderCost(price);
		cart.addItemToItemList(item);
		cartsService.saveOrUpdate(cart);
		
		customer.setCart(cart);
		customersService.saveOrUpdateCustomerWithoutUpdatingPassword(customer);
	}
	
	@RequestMapping(value = "/api/deleteCartItem", method = RequestMethod.POST)
	@ResponseBody
	public void deleteCartItem(@RequestBody String itemToDeleteJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(itemToDeleteJson, new TypeReference<HashMap<String, String>>() {
			});
			deleteItem(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void processPurchase(Map<String, String> map) {
		String username = (String) map.get("username");
		String address = (String) map.get("address");
		String city = (String) map.get("city");
		String county = (String) map.get("county");
		String country = (String) map.get("country");
		
		Customer customer = customersService.getCustomerByUsername(username);
		Cart cart = customer.getCart();
		double cost = cart.getOrderCost();
		List<PHLineItem> phls = new ArrayList<>();
		List<LineItem> items = cart.getItems();
		for(int i = 0; i < items.size(); i++) {
			Book savedBook = booksService.getBookByID(items.get(i).getBook().getId());
			int quantity = items.get(i).getQuantity();
			int id = items.get(i).getBook().getId();
			PHLineItem ph = new PHLineItem(quantity, savedBook);
			phls.add(ph);
			phLineItemsService.saveOrUpdate(ph);
		}	
		
		CalculateDiscountStrategy smDiscountStrategy = new SmallDiscountStrategy();
		CalculateDiscountStrategy lgDiscountStrategy = new LargeDiscountStrategy();

		double purchaseDiscount = 0.00;
		
		if(customer.getPurchasehistorys().size() > 3 && customer.getPurchasehistorys().size() < 6) {
			customer.setDiscountStrategy(smDiscountStrategy);
			purchaseDiscount = customer.applyDiscount();
		} else if(customer.getPurchasehistorys().size() > 6) {
			customer.setDiscountStrategy(lgDiscountStrategy);
			purchaseDiscount = customer.applyDiscount();
		}
		
		cost = cost - purchaseDiscount;
		
		PurchaseHistory purchaseHistory = new PurchaseHistory(new Date(), cost, address,
				city, county, country, phls);
		purchaseHistorysService.saveOrUpdate(purchaseHistory);
		
		List<PurchaseHistory> historys;
		if(customer.getPurchasehistorys() == null || customer.getPurchasehistorys().size() == 0) {
			historys = new ArrayList<>();
			historys.add(purchaseHistory);
		} else {
			historys = customer.getPurchasehistorys();
			historys.add(purchaseHistory);
		}
		
		customer.setPurchasehistorys(historys);
		customersService.saveOrUpdateCustomerWithoutUpdatingPassword(customer);
		
		
		double newPrice = 0.00;
		List<LineItem> newItemArray = new ArrayList<>();

		cart.setOrderCost(newPrice);
		cart.setItems(newItemArray);
		cartsService.saveOrUpdate(cart);
	}
	
	@RequestMapping(value = "/api/purchaseClicked", method = RequestMethod.POST)
	@ResponseBody
	public void purchaseClicked(@RequestBody String jsonData) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(jsonData, new TypeReference<HashMap<String, String>>() {
			});
			processPurchase(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void deleteItem(Map<String, String> map) {
		String username = (String) map.get("username");
		String idToConvert = (String) map.get("id");
		int id = Integer.parseInt(idToConvert);
		String bookpriceToConvert= (String) map.get("bookprice");
		double bookprice = Double.parseDouble(bookpriceToConvert);
		
		Book book = booksService.getBookByID(id);
		
		int quantityBooks = book.getBookQuantity();
		int remainingBooks = quantityBooks + 1;
		book.setBookQuantity(remainingBooks);
		booksService.saveOrUpdate(book);
		
		Customer customer = customersService.getCustomerByUsername(username);
		Cart cart = customer.getCart();
		
		List<LineItem> items = cart.getItems();
		for(int i = 0; i < items.size(); i++) {
			if(items.get(i).getBook().getId() == id) {
				items.get(i).setBook(null);
				items.remove(items.get(i));
			}
		}		
		
		double price = cart.getOrderCost();
		double newPrice = price - bookprice;
		
		cart.setItems(items);
		cart.setOrderCost(newPrice);
		cartsService.saveOrUpdate(cart);
		
	}
	
	@RequestMapping(value = "/api/minusButtonPressed", method = RequestMethod.POST)
	@ResponseBody
	public void minusButtonPressed(@RequestBody String changesInJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(changesInJson, new TypeReference<HashMap<String, String>>() {
			});
			saveMinusChanges(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void saveMinusChanges(Map<String, String> map) {
		String username = (String) map.get("username");
		String idToConvert = (String) map.get("id");
		int id = Integer.parseInt(idToConvert);
		String bookpriceToConvert= (String) map.get("bookprice");
		double bookprice = Double.parseDouble(bookpriceToConvert);
		
		Customer customer = customersService.getCustomerByUsername(username);
		Cart cart = customer.getCart();		
		Book book = booksService.getBookByID(id);
		
		int quantityBooks = book.getBookQuantity();
		int remainingBooks = quantityBooks + 1;
		book.setBookQuantity(remainingBooks);
		booksService.saveOrUpdate(book);
		
		List<LineItem> items = cart.getItems();
		for(LineItem item: items) {
			if(item.getBook().getId() == id) {
				item.setQuantity(item.getQuantity()-1);
				item.setBook(book);
				lineItemsService.saveOrUpdate(item);
			}
		}
		
		double price = cart.getOrderCost();
		double newPrice = price - bookprice;
		cart.setOrderCost(newPrice);
		cartsService.saveOrUpdate(cart);
	
		customer.setCart(cart);
		customersService.saveOrUpdateCustomerWithoutUpdatingPassword(customer);
	}
}
