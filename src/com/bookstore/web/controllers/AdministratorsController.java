package com.bookstore.web.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bookstore.web.dao.Administrator;
import com.bookstore.web.dao.Book;
import com.bookstore.web.dao.BroadcastMessage;
import com.bookstore.web.dao.Cart;
import com.bookstore.web.dao.Customer;
import com.bookstore.web.service.AdministratorsService;
import com.bookstore.web.service.BroadcastMessagesService;
import com.bookstore.web.service.CustomersService;

@Controller
public class AdministratorsController {
	
private BroadcastMessagesService broadcastmessagesService;
	
	@Autowired
	public void setBroadcastMessagesService(BroadcastMessagesService broadcastmessagesService) {
		this.broadcastmessagesService = broadcastmessagesService;
	}
	
	private AdministratorsService adminsService;
	
	@Autowired
	public void setAdministratorsService(AdministratorsService adminsService) {
		this.adminsService = adminsService;
	}
	
	@RequestMapping("/admin")
	public String showAdmin() {
		return "admin";
	}
	
	@RequestMapping("/customerswithpurchases")
	public String showCustomersWithPurchases() {
		return "customerswithpurchases";
	}
	
	@RequestMapping("/createadminaccount")
	public String createAdminAccount() {
		return "createadminaccount";
	}
	
	@RequestMapping("/viewallcustomers")
	public String viewAllCustomers() {
		return "viewallcustomers";
	}
	
	@RequestMapping(value = "/api/getLatestBroadcast", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public BroadcastMessage getLatestBroadcast() {
		return broadcastmessagesService.getLatestBroadcast();
	}
	
	@RequestMapping(value = "/api/retrieveBroadcastMessage", method = RequestMethod.POST)
	@ResponseBody
	public void retrieveBroadcastMessage(@RequestBody String msgJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(msgJson, new TypeReference<HashMap<String, String>>() {
			});
			saveBroadast(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/api/getadminbyusername", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Administrator getadminbyusername() {
		return adminsService.getAdministratorByUsername("superuser");
	}
	
	private void saveBroadast(Map<String, String> map) {
		String username = (String) map.get("username");
		String message = (String) map.get("bMessage");
		Administrator admin = adminsService.getAdministratorByUsername(username);
		List<BroadcastMessage> broadcasts = null;
		if(admin.getMessages() == null || admin.getMessages().size() == 0) {
			broadcasts =  new ArrayList<>();
		} else {
			broadcasts = admin.getMessages();
		}
		BroadcastMessage bm = new BroadcastMessage(new Date(), message);
		broadcastmessagesService.saveOrUpdate(bm);
		
		broadcasts.add(bm);
		admin.setMessages(broadcasts);
		adminsService.saveOrUpdateWithoutUpdatingPassword(admin);
	}

	@RequestMapping(value = "/api/createAdmin", method = RequestMethod.POST)
	@ResponseBody
	public void registerResident(@RequestBody String residentJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(residentJson, new TypeReference<HashMap<String, String>>() {
			});
			createAdmin(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createAdmin(Map<String, String> map) {
		String username = (String) map.get("adminname");
		String password = (String) map.get("password");
		String email = (String) map.get("email");
		boolean enabled = true;
		String authority = "ROLE_ADMIN";
		
		Administrator admin = new Administrator(username, password, enabled, authority, email, null);
		adminsService.saveOrUpdate(admin);
	}
}
