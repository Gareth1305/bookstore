package com.bookstore.web.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bookstore.web.dao.Customer;
import com.bookstore.web.dao.User;
import com.bookstore.web.service.UsersService;

@Controller
public class UsersController {

	private List<User> customersWithOrders = new ArrayList<User>();
	private UsersService usersService;
	
	@Autowired
	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}
	
	@RequestMapping(value = "/api/getAllCustomersWithOrders", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<User> getAllCustomersWithOrders() {
		customersWithOrders.clear();
		Iterator itr = usersService.createIterator();
		iterateList(itr);
		return customersWithOrders;
	}

	private List<User> iterateList(Iterator itr) {
		while (itr.hasNext()) {
			Customer customer = (Customer) itr.next();
			if(customer.getPurchasehistorys().size() != 0) {
				customersWithOrders.add(customer);
			}
		}
		return customersWithOrders;
	}
}
