package com.bookstore.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OrdersController {

	@RequestMapping("/viewcustomersorders")
	public String viewcustomersorders() {
		return "viewcustomersorders";
	}

}
