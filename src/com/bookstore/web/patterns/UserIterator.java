package com.bookstore.web.patterns;

import java.util.Iterator;

public interface UserIterator {
	public Iterator createIterator();
}
