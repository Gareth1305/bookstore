package com.bookstore.web.patterns;

public class Memento {

	private String title;
	private String author;
	private double price;
	private String genre;
	private int noInStock;
	
	public Memento(String title, String author, double price, String genre, int noInStock) {
		this.title = title;
		this.author = author;
		this.price = price;
		this.genre = genre;
		this.noInStock = noInStock;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public double getPrice() {
		return price;
	}

	public String getGenre() {
		return genre;
	}

	public int getNoInStock() {
		return noInStock;
	}

}
