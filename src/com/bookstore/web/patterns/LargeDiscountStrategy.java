package com.bookstore.web.patterns;

public class LargeDiscountStrategy implements CalculateDiscountStrategy {

	@Override
	public double calculateDiscount() {
		return 10.00;
	}

}
