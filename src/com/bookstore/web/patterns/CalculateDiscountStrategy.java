package com.bookstore.web.patterns;

public interface CalculateDiscountStrategy {
	public double calculateDiscount();
}
