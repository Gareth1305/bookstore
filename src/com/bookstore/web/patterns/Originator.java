package com.bookstore.web.patterns;

public class Originator {

	private String title;
	private String author;
	private double price;
	private String genre;
	private int noInStock;
	
	private String lastUndoSavepoint;
	CareTaker caretaker;
	
	public Originator(String title, String author, double price, String genre, int noInStock, CareTaker caretaker) {
		this.title = title;
		this.author = author;
		this.price = price;
		this.genre = genre;
		this.noInStock = noInStock;
		this.caretaker = caretaker;
		createSavepoint("INITIAL");
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getNoInStock() {
		return noInStock;
	}

	public void setNoInStock(int noInStock) {
		this.noInStock = noInStock;
	}

	public CareTaker getCaretaker() {
		return caretaker;
	}

	public void setCaretaker(CareTaker caretaker) {
		this.caretaker = caretaker;
	}
	
	public void createSavepoint(String savepointName){
		caretaker.saveMemento(new Memento(this.title, this.author, this.price, 
				this.genre, this.noInStock), savepointName);
		lastUndoSavepoint = savepointName;
	}
	
	public void undoAll(){
		setOriginatorState("INITIAL");
		caretaker.clearSavepoints();
	}
	
	private void setOriginatorState(String savepointName){
		Memento mem = caretaker.getMemento(savepointName);
		this.title = mem.getTitle();
		this.author = mem.getAuthor();
		this.price = mem.getPrice();
		this.genre = mem.getGenre();
		this.noInStock = mem.getNoInStock();
	}

	@Override
	public String toString() {
		return "Originator [title=" + title + ", author=" + author + ", price=" + price + ", genre=" + genre
				+ ", noInStock=" + noInStock + ", lastUndoSavepoint=" + lastUndoSavepoint + ", caretaker=" + caretaker
				+ "]";
	}

}
