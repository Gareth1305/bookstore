package com.bookstore.web.patterns;

public class SmallDiscountStrategy implements CalculateDiscountStrategy {

	@Override
	public double calculateDiscount() {
		return 5.00;
	}
}
