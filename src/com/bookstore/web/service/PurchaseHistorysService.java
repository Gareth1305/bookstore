package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.PurchaseHistory;
import com.bookstore.web.dao.PurchaseHistorysDao;

@Service("purchaseHistorysService")
public class PurchaseHistorysService {

	private PurchaseHistorysDao purchaseHistorysDao;

	@Autowired
	public void setPurchaseHistorysDao(PurchaseHistorysDao purchaseHistorysDao) {
		this.purchaseHistorysDao = purchaseHistorysDao;
	}

	public List<PurchaseHistory> getAllPurchaseHistorys() {
		return purchaseHistorysDao.getPurchaseHistorys();
	}

	public void saveOrUpdate(PurchaseHistory cart) {
		purchaseHistorysDao.saveOrUpdate(cart);
	}

	public PurchaseHistory getPurchaseHistoryByID(int cartId) {
		return purchaseHistorysDao.getPurchaseHistory(cartId);
	}

	public void deletePurchaseHistory(int id) {
		purchaseHistorysDao.delete(id);
	}
}
