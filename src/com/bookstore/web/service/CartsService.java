package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.Cart;
import com.bookstore.web.dao.CartsDao;

@Service("cartsService")
public class CartsService {
	
	private CartsDao cartsDao;

	@Autowired
	public void setCartsDao(CartsDao cartsDao) {
		this.cartsDao = cartsDao;
	}

	public List<Cart> getAllCarts() {
		return cartsDao.getCarts();
	}

	public void saveOrUpdate(Cart cart) {
		cartsDao.saveOrUpdate(cart);
	}

	public Cart getCartByID(int cartId) {
		return cartsDao.getCart(cartId);
	}

	public void deleteCart(int id) {
		cartsDao.delete(id);
	}
}
