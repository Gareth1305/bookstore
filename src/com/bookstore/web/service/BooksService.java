package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.Book;
import com.bookstore.web.dao.BooksDao;

@Service("booksService")
public class BooksService {
	private BooksDao booksDao;

	@Autowired
	public void setBooksDao(BooksDao booksDao) {
		this.booksDao = booksDao;
	}

	public List<Book> getAllBooks() {
		return booksDao.getBooks();
	}

	public void saveOrUpdate(Book book) {
		booksDao.saveOrUpdate(book);
	}

	public Book getBookByID(int bookId) {
		return booksDao.getBook(bookId);
	}

	public long getNumberOfBooks() {
		return booksDao.getNumberOfBooks();
	}

	public List<Book> getUpdatedBooks(int limit) {
		return booksDao.getUpdatedBooks(limit);
	}

	public List<Book> getAllAvailableBooks() {
		return booksDao.getAllAvailableBooks();
	}

	public void deleteBook(int id) {
		booksDao.delete(id);
	}
}
