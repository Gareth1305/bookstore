package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.Administrator;
import com.bookstore.web.dao.AdministratorsDao;

@Service("administratorsService")
public class AdministratorsService {
	
	private AdministratorsDao administratorsDao;
	
	@Autowired
	public void setAdministratorsDao(AdministratorsDao administratorsDao) {
		this.administratorsDao = administratorsDao;
	}
	
	public void saveOrUpdate(Administrator admin) {
		administratorsDao.saveOrUpdate(admin);
	}
	
	public List<Administrator> getCurrent() {
		return administratorsDao.getAdministrators();
	}
	
	public void deleteAdministratorByUsername(String username) {
		administratorsDao.delete(username);
	}
	
	public Administrator getAdministratorByUsername(String username) {
		return administratorsDao.getAdministrator(username);
	}

	public void saveOrUpdateWithoutUpdatingPassword(Administrator admin) {
		administratorsDao.saveOrUpdateWithoutUpdatingPassword(admin);
	}
}
