package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.Review;
import com.bookstore.web.dao.ReviewsDao;

@Service("reviewsService")
public class ReviewsService {

	private ReviewsDao reviewsDao;

	@Autowired
	public void setReviewsDao(ReviewsDao reviewsDao) {
		this.reviewsDao = reviewsDao;
	}
	
	public List<Review> getAllReviews() {
		return reviewsDao.getReviews();
	}

	public void createReview(Review review) {
		reviewsDao.saveOrUpdate(review);
	}

	public void deleteReviewById(int id) {
		reviewsDao.delete(id);
	}

	public Review getReviewById(int id) {
		return reviewsDao.getReview(id);
	}

	public void saveOrUpdateReview(Review review) {
		reviewsDao.saveOrUpdate(review);
	}
	
}
