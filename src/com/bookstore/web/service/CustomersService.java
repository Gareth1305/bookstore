package com.bookstore.web.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.Cart;
import com.bookstore.web.dao.Customer;
import com.bookstore.web.dao.CustomersDao;
import com.bookstore.web.dao.PurchaseHistory;

@Service("customersService")
public class CustomersService {

	private CustomersDao customersDao;

	@Autowired
	public void setCustomersDao(CustomersDao customersDao) {
		this.customersDao = customersDao;
	}
	
	public ArrayList<Customer> getCustomers() {
		return customersDao.getCustomers();
	}
	
	public void createCustomer(Customer customer) {
		customersDao.create(customer);
	}
	
	public void deleteCustomerByUsername(String username) {
		customersDao.delete(username);
	}
	
	public Customer getCustomerByUsername(String username) {
		return customersDao.getCustomer(username);
	}

	public void saveOrUpdate(Customer customer) {
		customersDao.saveOrUpdate(customer);
	}

	public Cart getShoppingCart(String username) {
		return customersDao.getShoppingCart(username);
	}

	public void saveOrUpdateCustomerWithoutUpdatingPassword(Customer customer) {
		customersDao.saveOrUpdateCustomerWithoutUpdatingPassword(customer);
	}

	public List<PurchaseHistory> getHistoryByUsername(String username) {
		return customersDao.getHistoryByUsername(username);
	}
}
