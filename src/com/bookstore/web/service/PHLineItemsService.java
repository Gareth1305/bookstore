package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.Cart;
import com.bookstore.web.dao.CartsDao;
import com.bookstore.web.dao.LineItem;
import com.bookstore.web.dao.LineItemsDao;
import com.bookstore.web.dao.PHLineItem;
import com.bookstore.web.dao.PHLineItemsDao;

@Service("phLineItemsService")
public class PHLineItemsService {
	
	private PHLineItemsDao phLineItemsDao;

	@Autowired
	public void setLineItemsDao(PHLineItemsDao phLineItemsDao) {
		this.phLineItemsDao = phLineItemsDao;
	}

	public List<PHLineItem> getAllPHLineItems() {
		return phLineItemsDao.getPHLineItems();
	}

	public void saveOrUpdate(PHLineItem item) {
		phLineItemsDao.saveOrUpdate(item);
	}

	public PHLineItem getPHLineItemByID(int cartId) {
		return phLineItemsDao.getPHLineItem(cartId);
	}

	public void deletePHLineItem(int id) {
		phLineItemsDao.delete(id);
	}

	public PHLineItem getLineItemByBookId(int id) {
		return phLineItemsDao.getPHLineItemByBookId(id);
	}
}
