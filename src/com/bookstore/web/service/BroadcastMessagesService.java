package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.BroadcastMessage;
import com.bookstore.web.dao.BroadcastMessagesDao;

@Service("broadcastMessagesService")
public class BroadcastMessagesService {

	private BroadcastMessagesDao broadcastMessagesDao;

	@Autowired
	public void setBroadcastMessagesDao(BroadcastMessagesDao broadcastMessagesDao) {
		this.broadcastMessagesDao = broadcastMessagesDao;
	}
	
	public List<BroadcastMessage> getAllBroadcastMessages() {
		return broadcastMessagesDao.getBroadcastMessages();
	}

	public void saveOrUpdate(BroadcastMessage broadcastMessage) {
		broadcastMessagesDao.saveOrUpdate(broadcastMessage);
	}

	public BroadcastMessage getLatestBroadcast() {
		return broadcastMessagesDao.getLatestBroadcast();
	}	
}
