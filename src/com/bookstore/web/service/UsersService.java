package com.bookstore.web.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.User;
import com.bookstore.web.dao.UsersDao;
import com.bookstore.web.patterns.UserIterator;

@Service("usersService")
public class UsersService implements UserIterator {

	private List<User> users = new ArrayList<>();
	private UsersDao usersDao;
	
	@Autowired
	public void setUsersDao(UsersDao usersDao) {
		this.usersDao = usersDao;
	}
	
	public void createUser(User user) {
		usersDao.create(user);
	}
	
	public List<User> getAllUsers() {
		return usersDao.getAllUsers();
	}

	@Override
	public Iterator createIterator() {
		users = usersDao.getAllCustomers();
		return users.iterator();
	}


	
}
