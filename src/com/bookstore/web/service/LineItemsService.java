package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.Cart;
import com.bookstore.web.dao.CartsDao;
import com.bookstore.web.dao.LineItem;
import com.bookstore.web.dao.LineItemsDao;

@Service("lineItemsService")
public class LineItemsService {
	
	private LineItemsDao lineItemsDao;

	@Autowired
	public void setLineItemsDao(LineItemsDao lineItemsDao) {
		this.lineItemsDao = lineItemsDao;
	}

	public List<LineItem> getAllLineItems() {
		return lineItemsDao.getLineItems();
	}

	public void saveOrUpdate(LineItem cart) {
		lineItemsDao.saveOrUpdate(cart);
	}

	public LineItem getLineItemByID(int cartId) {
		return lineItemsDao.getLineItem(cartId);
	}

	public void deleteLineItem(int id) {
		lineItemsDao.delete(id);
	}

	public LineItem getLineItemByBookId(int id) {
		return lineItemsDao.getLineItemByBookId(id);
	}
}
