package com.bookstore.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("administratorsDao")
public class AdministratorsDao {

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<Administrator> getAdministrators() {
		return session().createQuery("from Administrator").list();
	}
	
	public void saveOrUpdate(Administrator administrator) {
		administrator.setPassword(passwordEncoder.encode(administrator.getPassword()));
		session().saveOrUpdate(administrator);
	}

	public void saveOrUpdateWithoutUpdatingPassword(Administrator administrator) {
		session().saveOrUpdate(administrator);
	}
	
	public void delete(String username) {
		Query query = session().createQuery("delete from Administrator where username=:username");
		query.setString("username", username);
		Query query2 = session().createQuery("delete from User where username=:username");
		query2.setString("username", username);
		query.executeUpdate();
		query2.executeUpdate();
	}
	
	public Administrator getAdministrator(String username) {
		Criteria crit = session().createCriteria(Administrator.class);
		crit.add(Restrictions.idEq(username));
		crit.add(Restrictions.eq("username", username));
		return (Administrator)crit.uniqueResult();
	}

}