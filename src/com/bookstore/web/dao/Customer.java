package com.bookstore.web.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.bookstore.web.patterns.CalculateDiscountStrategy;

@Entity
@Table(name = "customers")
public class Customer extends User {

	private String address;
	private String city;
	private String county;
	private String country;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Cart cart;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<PurchaseHistory> purchasehistorys;
	@Transient
	private CalculateDiscountStrategy discountStrategy;
	
	public CalculateDiscountStrategy getDiscountStrategy() {
		return discountStrategy;
	}

	public void setDiscountStrategy(CalculateDiscountStrategy discountStrategy) {
		this.discountStrategy = discountStrategy;
	}

	public double applyDiscount() {
		return discountStrategy.calculateDiscount();
	}
	
	private Customer() {
		super();
	}

	public Customer(String username, String password, boolean enabled, String authority, String email,
			String address, String city, String county, String country, Cart cart) {
		super(username, password, enabled, authority, email);
		this.address = address;
		this.city = city;
		this.county = county;
		this.country = country;
		this.cart = cart;
	}
	
	public Customer(String username, String password, boolean enabled, String authority, String email,
			String address, String city, String county, String country, Cart cart,
			List<PurchaseHistory> purchasehistorys) {
		super(username, password, enabled, authority, email);
		this.address = address;
		this.city = city;
		this.county = county;
		this.country = country;
		this.cart = cart;
		this.purchasehistorys = purchasehistorys;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

	public List<PurchaseHistory> getPurchasehistorys() {
		return purchasehistorys;
	}

	public void setPurchasehistorys(List<PurchaseHistory> purchasehistorys) {
		this.purchasehistorys = purchasehistorys;
	}
}
