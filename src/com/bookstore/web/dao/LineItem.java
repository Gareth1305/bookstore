package com.bookstore.web.dao;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name = "lineItems")
public class LineItem {
	@Id
	@GeneratedValue
	private int id;
	private int quantity;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Book book;

	public LineItem() {
	}

	public LineItem(int quantity, Book book) {
		this.quantity = quantity;
		this.book = book;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		System.out.println("In setter: " + quantity);
		this.quantity = quantity;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

}
