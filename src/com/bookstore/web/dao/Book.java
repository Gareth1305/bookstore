package com.bookstore.web.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "books")
public class Book {

	@Id
	@GeneratedValue
	private int id;
	private String bookTitle;
	private String bookAuthor;
	private String bookGenre;
	private double bookPrice;
	private String bookImageTitle;
	private int bookQuantity;
	@OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference
	private List<Review> reviews;

	public Book() {

	}

	public Book(String bookTitle, String bookAuthor, String bookGenre, double bookPrice, String bookImageTitle,
			int bookQuantity, List<Review> reviews) {
		this.bookTitle = bookTitle;
		this.bookAuthor = bookAuthor;
		this.bookGenre = bookGenre;
		this.bookPrice = bookPrice;
		this.bookImageTitle = bookImageTitle;
		this.bookQuantity = bookQuantity;
		this.reviews = reviews;
	}

	public Book(String bookTitle, String bookAuthor, String bookGenre, double bookPrice, String bookImageTitle,
			int bookQuantity) {
		this.bookTitle = bookTitle;
		this.bookAuthor = bookAuthor;
		this.bookGenre = bookGenre;
		this.bookPrice = bookPrice;
		this.bookImageTitle = bookImageTitle;
		this.bookQuantity = bookQuantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	public String getBookGenre() {
		return bookGenre;
	}

	public void setBookGenre(String bookGenre) {
		this.bookGenre = bookGenre;
	}

	public double getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(double bookPrice) {
		this.bookPrice = bookPrice;
	}

	public String getBookImageTitle() {
		return bookImageTitle;
	}

	public void setBookImageTitle(String bookImageTitle) {
		this.bookImageTitle = bookImageTitle;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public int getBookQuantity() {
		return bookQuantity;
	}

	public void setBookQuantity(int bookQuantity) {
		System.out.println("In setter... " + bookQuantity);
		this.bookQuantity = bookQuantity;
	}

}