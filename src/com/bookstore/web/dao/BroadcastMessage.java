package com.bookstore.web.dao;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="broadcastmessages")
public class BroadcastMessage {

	@Id
	@GeneratedValue
	private int id;
	private Date broadcastDate;
	private String message;
		
	public BroadcastMessage() {
	
	}

	public BroadcastMessage(Date broadcastDate, String message) {
		this.broadcastDate = broadcastDate;
		this.message = message;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBroadcastDate() {
		return broadcastDate;
	}

	public void setBroadcastDate(Date broadcastDate) {
		this.broadcastDate = broadcastDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "BroadcastMessage [id=" + id + ", broadcastDate=" + broadcastDate + ", message=" + message + "]";
	}
	
}
