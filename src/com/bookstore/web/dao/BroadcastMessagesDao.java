package com.bookstore.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("broadcastMessagesDao")
public class BroadcastMessagesDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<BroadcastMessage> getBroadcastMessages() {
		return session().createQuery("from BroadcastMessage").list();
	}
	
	public void saveOrUpdate(BroadcastMessage broadcastMessage) {
		session().saveOrUpdate(broadcastMessage);
	}

	public BroadcastMessage getLatestBroadcast() {
		Criteria crit = session().createCriteria(BroadcastMessage.class);
		crit.addOrder(Order.desc("broadcastDate"));
		crit.setMaxResults(1);
		return (BroadcastMessage) crit.uniqueResult();
	}
}
