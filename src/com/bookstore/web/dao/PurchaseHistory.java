package com.bookstore.web.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "purchaseHistory")
public class PurchaseHistory {
	@Id
	@GeneratedValue
	private int id;
	private Date date;
	private double cost;
	private String shipAddress;
	private String shipCity;
	private String shipCounty;
	private String shipCountry;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<PHLineItem> phLineItems;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public List<PHLineItem> getPhLineItems() {
		return phLineItems;
	}

	public void setPhLineItems(List<PHLineItem> phLineItems) {
		this.phLineItems = phLineItems;
	}

	public PurchaseHistory() {
	}

	public PurchaseHistory(Date date, double cost, String shipAddress, String shipCity, String shipCounty,
			String shipCountry, List<PHLineItem> phLineItems) {
		this.date = date;
		this.cost = cost;
		this.shipAddress = shipAddress;
		this.shipCity = shipCity;
		this.shipCounty = shipCounty;
		this.shipCountry = shipCountry;
		this.phLineItems = phLineItems;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getShipAddress() {
		return shipAddress;
	}

	public void setShipAddress(String shipAddress) {
		this.shipAddress = shipAddress;
	}

	public String getShipCity() {
		return shipCity;
	}

	public void setShipCity(String shipCity) {
		this.shipCity = shipCity;
	}

	public String getShipCounty() {
		return shipCounty;
	}

	public void setShipCounty(String shipCounty) {
		this.shipCounty = shipCounty;
	}

	public String getShipCountry() {
		return shipCountry;
	}

	public void setShipCountry(String shipCountry) {
		this.shipCountry = shipCountry;
	}

}
