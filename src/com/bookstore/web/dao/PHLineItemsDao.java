package com.bookstore.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("phLineItemsDao")
public class PHLineItemsDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<PHLineItem> getPHLineItems() {
		return session().createQuery("from PHLineItem").list();
	}
	
	public void saveOrUpdate(PHLineItem lineItem) {
		session().saveOrUpdate(lineItem);
	}
	
	public void delete(int id) {
		Query query = session().createQuery("delete from PHLineItem where id=:id");
		query.setLong("id", id);
		query.executeUpdate();
	}
	
	public PHLineItem getPHLineItem(int id) {
		Criteria crit = session().createCriteria(PHLineItem.class);
		crit.add(Restrictions.idEq(id));
		return (PHLineItem)crit.uniqueResult();
	}

	public PHLineItem getPHLineItemByBookId(int id) {
		Criteria crit = session().createCriteria(PHLineItem.class);
		crit.add(Restrictions.eq("book.id", id));
		return (PHLineItem)crit.uniqueResult();
	}
	
}
