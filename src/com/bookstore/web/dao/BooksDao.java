package com.bookstore.web.dao;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("booksDao")
public class BooksDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public List<Book> getBooks() {
		return session().createQuery("from Book").list();
	}

	public void saveOrUpdate(Book book) {
		session().saveOrUpdate(book);
	}

	public boolean delete(int id) {
		Query query = session().createQuery("delete from Book where id=:id");
		query.setLong("id", id);
		return query.executeUpdate() == 1;
	}

	public Book getBook(int id) {
		Criteria crit = session().createCriteria(Book.class);
		crit.add(Restrictions.idEq(id));
		return (Book)crit.uniqueResult();
	}

	public long getNumberOfBooks() {
		Query query = session().createQuery("select count(*) from Book");
		return (long) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Book> getUpdatedBooks(int limit) {
		Criteria crit = session().createCriteria(Book.class);
		crit.addOrder(Order.desc("id"));
		crit.setMaxResults(limit);
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Book> getAllAvailableBooks() {
		Criteria crit = session().createCriteria(Book.class);
		crit.add(Restrictions.eq("bookIsSold", false));
		return crit.list();
	}
}