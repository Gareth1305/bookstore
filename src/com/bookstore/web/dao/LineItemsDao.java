package com.bookstore.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("lineItemsDao")
public class LineItemsDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<LineItem> getLineItems() {
		return session().createQuery("from LineItem").list();
	}
	
	public void saveOrUpdate(LineItem lineItem) {
		session().saveOrUpdate(lineItem);
	}
	
	public void delete(int id) {
		Query query = session().createQuery("delete from LineItem where id=:id");
		query.setLong("id", id);
		query.executeUpdate();
	}
	
	public LineItem getLineItem(int id) {
		Criteria crit = session().createCriteria(LineItem.class);
		crit.add(Restrictions.idEq(id));
		return (LineItem)crit.uniqueResult();
	}

	public LineItem getLineItemByBookId(int id) {
		Criteria crit = session().createCriteria(LineItem.class);
		crit.add(Restrictions.eq("book.id", id));
		return (LineItem)crit.uniqueResult();
	}
	
}
