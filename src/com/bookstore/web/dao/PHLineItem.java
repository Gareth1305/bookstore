package com.bookstore.web.dao;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "phLineItems")
public class PHLineItem {

	@Id
	@GeneratedValue
	private int id;
	private int quantity;
	@ManyToOne
	private Book book;
	
	public PHLineItem() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public PHLineItem(int quantity, Book book) {
		this.quantity = quantity;
		this.book = book;
	}

}
