package com.bookstore.web.dao;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("reviewsDao")
public class ReviewsDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<Review> getReviews() {
		return session().createQuery("from Review").list();
	}

	public void saveOrUpdate(Review review) {
		session().saveOrUpdate(review);
	}

	public void delete(int id) {
		Query query = session().createQuery("delete from Review where id=:id");
		query.setLong("id", id);
		query.executeUpdate();
	}

	public Review getReview(int id) {
		Criteria crit = session().createCriteria(Review.class);
		crit.add(Restrictions.idEq(id));
		return (Review)crit.uniqueResult();
	}
}
