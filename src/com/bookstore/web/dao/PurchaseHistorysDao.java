package com.bookstore.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("purchaseHistorysDao")
public class PurchaseHistorysDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<PurchaseHistory> getPurchaseHistorys() {
		return session().createQuery("from PurchaseHistory").list();
	}
	
	public void saveOrUpdate(PurchaseHistory purchaseHistory) {
		session().saveOrUpdate(purchaseHistory);
	}
	
	public void delete(int id) {
		Query query = session().createQuery("delete from PurchaseHistory where id=:id");
		query.setLong("id", id);
		query.executeUpdate();
	}
	
	public PurchaseHistory getPurchaseHistory(int id) {
		Criteria crit = session().createCriteria(PurchaseHistory.class);
		crit.add(Restrictions.idEq(id));
		return (PurchaseHistory)crit.uniqueResult();
	}
	
}
