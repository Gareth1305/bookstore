package com.bookstore.web.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "administrators")
public class Administrator extends User {
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<BroadcastMessage> messages;
	
	public List<BroadcastMessage> getMessages() {
		return messages;
	}

	public void setMessages(List<BroadcastMessage> messages) {
		this.messages = messages;
	}

	public Administrator() {

	}

	public Administrator(String username, String password, boolean enabled, String authority, String email,
			List<BroadcastMessage> messages) {
		super(username, password, enabled, authority, email);
		this.messages = messages;
	}

	
	
	@Override
	public String toString() {
		return "Administrator [username=" + getUsername() + ", isEnabled()=" + isEnabled() + ", getAuthority()="
				+ getAuthority() + ", getEmail()=" + getEmail() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}

}
