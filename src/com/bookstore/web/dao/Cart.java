package com.bookstore.web.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="customersCart")
public class Cart {

	@Id
	@GeneratedValue
	private int id;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinTable(name="customers_cart_items")
    @JsonManagedReference
	private List<LineItem> items;
	private double orderCost;
	
	public Cart(List<LineItem> items, double orderCost) {
		this.items = items;
		this.orderCost = orderCost;
	}

	public Cart() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<LineItem> getItems() {
		return items;
	}

	public void setItems(List<LineItem> items) {
		this.items = items;
	}

	
	public void addItemToItemList(LineItem item) {
		if(getItems().size() == 0) {
			System.out.println("LineItems Is Empty");
			System.out.println("Creating New LineItem Array");
			ArrayList<LineItem> lItems = new ArrayList<>();
			lItems.add(item);
			setItems(lItems);
		} else {
			System.out.println("LineItems list is not empty");
			System.out.println("LineItems size is... " + getItems().size());
			List<LineItem> lItems = getItems();
			lItems.add(item);
			setItems(lItems);
		}
	}
	
	
	public double getOrderCost() {
		return orderCost;
	}

	public void setOrderCost(double orderCost) {
		this.orderCost = orderCost;
	}
	
}
