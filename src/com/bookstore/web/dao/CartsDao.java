package com.bookstore.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("cartsDao")
public class CartsDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<Cart> getCarts() {
		return session().createQuery("from Cart").list();
	}
	
	public void saveOrUpdate(Cart cart) {
		session().saveOrUpdate(cart);
	}
	
	public void delete(int id) {
		Query query = session().createQuery("delete from Cart where id=:id");
		query.setLong("id", id);
		query.executeUpdate();
	}
	
	public Cart getCart(int id) {
		Criteria crit = session().createCriteria(Cart.class);
		crit.add(Restrictions.idEq(id));
		return (Cart)crit.uniqueResult();
	}
}
