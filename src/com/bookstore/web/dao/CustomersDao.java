package com.bookstore.web.dao;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("customersDao")
public class CustomersDao {

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Customer> getCustomers() {
		ArrayList<Customer> custs = (ArrayList<Customer>) session().createQuery("from Customer").list();
		return custs;
	}
	
	public void saveOrUpdate(Customer customer) {
		customer.setPassword(passwordEncoder.encode(customer.getPassword()));
		session().saveOrUpdate(customer);
	}

	public void saveOrUpdateCustomerWithoutUpdatingPassword(Customer customer) {
		session().saveOrUpdate(customer);
	}
	
	public void create(Customer customer) {
		customer.setPassword(passwordEncoder.encode(customer.getPassword()));
		session().save(customer);
	}
	
	public void delete(String username) {
		Query query = session().createQuery("delete from Customer where username=:username");
		query.setString("username", username);
		query.executeUpdate();
	}

	public Customer getCustomer(String username) {
		Criteria crit = session().createCriteria(Customer.class);
		crit.add(Restrictions.eq("username", username));
		Customer customer = (Customer) crit.uniqueResult();
		return customer;
	}

	public Cart getShoppingCart(String username) {
		Criteria crit = session().createCriteria(Customer.class);
		crit.add(Restrictions.eq("username", username));
		Customer customer = (Customer) crit.uniqueResult();
		Cart cart = customer.getCart();
		return cart;
	}

	public List<PurchaseHistory> getHistoryByUsername(String username) {
		ArrayList<Customer> custs = (ArrayList<Customer>) session().createQuery("from Customer").list();
		List<PurchaseHistory> phArrayToReturn = null;
		for(Customer customer: custs) {
			if(customer.getUsername().equalsIgnoreCase(username)){
				phArrayToReturn = customer.getPurchasehistorys();
			}
		}
		return phArrayToReturn;
	}
}