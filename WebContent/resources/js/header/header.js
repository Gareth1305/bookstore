var app = angular.module("broadcastPage", []);

app.controller('broadcastCtrl', function($scope, $http, $window, $location) {
	$scope.bMessage = "";
	$scope.submit = function() {
		console.log($scope.bMessage);
		console.log($scope.currentUser);
		
		var msgToSend = {};
		msgToSend.username = $scope.currentUser;
		msgToSend.bMessage = $scope.bMessage;
		$http.post('http://localhost:8080/bookstore/api/retrieveBroadcastMessage', JSON.stringify(msgToSend))
        .success(function (data, status, headers, config) {
        	if(status == 200) {
        		console.log("Redirect to a new page");
        	}
        })
		
	}
});