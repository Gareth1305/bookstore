var viewCustomersOrders = angular.module("viewCustomersOrders", []);

viewCustomersOrders.controller('ViewCustomersOrdersCtrl', ['$scope', '$http', function($scope, $http) {

	$scope.customersWithOrders = [];
	
	$scope.loadData = function() {
		$http.get('http://localhost:8080/bookstore/api/getAllCustomersWithOrders').success(function(customers) {			
			$('#loader').hide();
		    $('#userList').show();
		    var iter = new Iterator(customers);
			for (var item = iter.first(); iter.hasNext(); item = iter.next()) { 
				$scope.customersWithOrders.push(item)
		    }	
		});
	}

	//Iterator Pattern
	var Iterator = function(items) {
		this.index = 0;
		this.items = items;
	}
	
	Iterator.prototype = {
		first: function() { 
			this.reset(); 
			return this.next(); 
		}, 
	    next: function() { 
	    	return this.items[this.index++]; 
	    }, 
	    hasNext: function() { 
	    	return this.index <= this.items.length; 
	    }, 
	    reset: function() { 
	    	this.index = 0; 
	    }, 
	    each: function(callback) { 
			for (var item = this.first(); this.hasNext(); item = this.next()) { 
			     callback(item); 
			} 
	    } 
	}
			
}]);