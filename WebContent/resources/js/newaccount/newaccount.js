var app = angular.module("newAccountPage", ['jcs-autoValidate']);

app.controller('NewAccountCtrl', function($scope, $http, $window, $location) {
	$scope.formModel = {};
	$scope.formModel.password = "";
	$scope.formModel.confirmpass = "";
	console.log("NEW ACC")
	$scope.onSubmit = function() {
		var resObjToSend = {};
		resObjToSend.customername = $scope.formModel.customername;
		resObjToSend.email = $scope.formModel.email;
		resObjToSend.password = $scope.formModel.password;
		resObjToSend.address = $scope.formModel.address;
		resObjToSend.city = $scope.formModel.city;
		resObjToSend.county = $scope.formModel.county;
		resObjToSend.country = $scope.formModel.country;
		console.log(resObjToSend);
		$http.post('http://localhost:8080/bookstore/api/createCustomer',
			JSON.stringify(resObjToSend)).success(
			function(data, status, headers, config) {
				if (status == 200) {
					$window.location.href = "http://localhost:8080/bookstore/";
				}
			});
	}
});