var app = angular.module("newBooksPage", ['jcs-autoValidate']);

app.controller('NewBooksCtrl', function($scope, $http, $window, $location) {
	$scope.formModel = {};
	console.log("NEW ACC")
	$scope.onSubmit = function() {
		var resObjToSend = {};
		resObjToSend.booktitle = $scope.formModel.booktitle;
		resObjToSend.bookauthor = $scope.formModel.bookauthor;
		resObjToSend.bookgenre = $scope.formModel.bookgenre;
		resObjToSend.bookprice = $scope.formModel.bookprice;
		resObjToSend.bookimagetitle = $scope.formModel.bookimagetitle;
		resObjToSend.bookquantity = $scope.formModel.bookquantity;
		console.log(resObjToSend);
		$http.post('http://localhost:8080/bookstore/api/createBook',
			JSON.stringify(resObjToSend)).success(
			function(data, status, headers, config) {
				if (status == 200) {
					$window.location.href = "http://localhost:8080/bookstore/booksadminview";
				}
			});
	}
});