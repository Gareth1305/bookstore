var app = angular.module("newAdminAccountPage", ['jcs-autoValidate']);

app.controller('NewAdminAccountCtrl', function($scope, $http, $window, $location) {
	$scope.formModel = {};
	$scope.formModel.password = "";
	$scope.formModel.confirmpass = "";
	$scope.onSubmit = function() {
		var resObjToSend = {};
		resObjToSend.adminname = $scope.formModel.adminname;
		resObjToSend.email = $scope.formModel.email;
		resObjToSend.password = $scope.formModel.password;
		console.log(resObjToSend);
		$http.post('http://localhost:8080/bookstore/api/createAdmin',
			JSON.stringify(resObjToSend)).success(
			function(data, status, headers, config) {
				if (status == 200) {
//					$window.location.href = "http://localhost:8080/bookstore/";
				}
			});
	}
});