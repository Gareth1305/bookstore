var myApp = angular.module("adminBookView", []);

myApp.controller('AdminBookCtrl', ['$scope', '$http', function($scope, $http, $dialogs) {
		
	$scope.sortType = 'title';
	$scope.sortReverse = false;
	$scope.searchBook = '';
	$scope.currentPage = 0;
	$scope.pageSize = 12;
	$scope.allBooks = [];
	$scope.activeBookToUpdate = {};
	$scope.activeBookToDelete = {};
	$scope.formFieldsActive = false;
	 $scope.checkboxModel = {
	       value1 : true
	 }
	//Iterator Pattern
	var Iterator = function(items) {
		this.index = 0;
		this.items = items;
	}
	
	Iterator.prototype = {
		first: function() { 
			this.reset(); 
			return this.next(); 
		}, 
	    next: function() { 
	    	return this.items[this.index++]; 
	    }, 
	    hasNext: function() { 
	    	return this.index <= this.items.length; 
	    }, 
	    reset: function() { 
	    	this.index = 0; 
	    }, 
	    each: function(callback) { 
			for (var item = this.first(); this.hasNext(); item = this.next()) { 
			     callback(item); 
			} 
	    } 
	}

	$scope.numberOfPages = function() {
		return Math.ceil($scope.allBooks.length / $scope.pageSize);
	};
	
	$http.get('http://localhost:8080/bookstore/api/getAllBooks').success(function(books) {
		var iter = new Iterator(books);
		for (var item = iter.first(); iter.hasNext(); item = iter.next()) { 
			$scope.allBooks.push(item)
	    }
	});
	
	$scope.deleteBook = function(book) {
		$scope.activeBookToDelete = book;
	}
	
	$scope.confirmedDelete = function() {
		$http.post('http://localhost:8080/bookstore/api/deleteBook',
			JSON.stringify($scope.activeBookToDelete)).success(
			function(data, status, headers, config) {
				if (status == 200) {					
					console.log("Success");
				}
			});
		var i = $scope.allBooks.indexOf($scope.activeBookToDelete);
		if(i != -1) {
			$scope.allBooks.splice(i, 1);
		}
	}
	
	$scope.updateBook = function(book) {
		$scope.formFieldsActive = false;
		$scope.activeBookToUpdate = book;
	}
	
	$scope.saveChanges = function() {
		
		var bookToUpdate = {};
		bookToUpdate.id = $scope.activeBookToUpdate.id;
		bookToUpdate.bookTitle = $scope.activeBookToUpdate.bookTitle;
		bookToUpdate.bookAuthor = $scope.activeBookToUpdate.bookAuthor;
		bookToUpdate.bookGenre = $scope.activeBookToUpdate.bookGenre;
		bookToUpdate.bookPrice = $scope.activeBookToUpdate.bookPrice;
		bookToUpdate.bookQuantity = $scope.activeBookToUpdate.bookQuantity;
		bookToUpdate.confirmVal = $scope.checkboxModel.value1;
		
		$http.post('http://localhost:8080/bookstore/api/updateBook',
			JSON.stringify(bookToUpdate)).success(
			function(data, status, headers, config) {
				if (status == 200) {
					$scope.formFieldsActive = true;
					console.log("Success");
				}
			});
	}
	
}]);

myApp.filter('startFrom', function() {
	return function(input, start) {
		start = +start; // parse to int
		return input.slice(start);
	}
});