var app = angular.module("viewAllCustomersPage", []);

app.controller('ViewAllCustomersCtrl', function($scope, $http, $window, $location) {
	
	$scope.customers = [];
	$scope.activeCustomer = {};
	
	$http.get('http://localhost:8080/bookstore/api/getAllCustomers').success(function(customers) {
		var iter = new Iterator(customers);
		for (var item = iter.first(); iter.hasNext(); item = iter.next()) { 
			$scope.customers.push(item)
	    }
	});
	
	$scope.viewOrders = function(customer) {
		console.log(customer)
		$scope.activeCustomer = customer;
	};
	
	//Iterator Pattern
	var Iterator = function(items) {
		this.index = 0;
		this.items = items;
	}
	
	Iterator.prototype = {
		first: function() { 
			this.reset(); 
			return this.next(); 
		}, 
	    next: function() { 
	    	return this.items[this.index++]; 
	    }, 
	    hasNext: function() { 
	    	return this.index <= this.items.length; 
	    }, 
	    reset: function() { 
	    	this.index = 0; 
	    }, 
	    each: function(callback) { 
			for (var item = this.first(); this.hasNext(); item = this.next()) { 
			     callback(item); 
			} 
	    } 
	}
	
});