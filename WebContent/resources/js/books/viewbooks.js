var viewAllBooks = angular.module("viewAllBooks", []);

viewAllBooks.factory('NotificationService', function($http) {
    var _subscribers = [];
    var messageObj = {};

    $http.get('http://localhost:8080/bookstore/api/getLatestBroadcast').success(function(messages) {
		messageObj.id = messages.id;
		messageObj.message = messages.message;
	});
    
    setInterval(function () {
        // Every 10 seconds, notify all of the subscribers
    	var isSameMessage = true;
    	
    	$http.get('http://localhost:8080/bookstore/api/getLatestBroadcast').success(function(messages) {
    		if(messageObj.id == messages.id) {
    			isSameMessage = true;
    		} else {
    			isSameMessage = false;
    			messageObj.id = messages.id;
        		messageObj.message = messages.message;
    		}
    	});
        setTimeout(function() {
        	console.log(isSameMessage);
        	if(isSameMessage == false) {
        		angular.forEach(_subscribers, function (cb) {
            		cb('ATTENTION! - ' + messageObj.message);
            	});
        	}
        }, 500);
    }, 10000);

    return {
        subscribe: function (cb) {
            _subscribers.push(cb);
        }
    };
});




viewAllBooks.controller('ViewAllBooksCtrl', ['$scope', '$http', '$timeout', 'NotificationService', function($scope, $http, $timeout, NotificationService) {

    $scope.notifications = ['nothing yet'];

    NotificationService.subscribe(function (notification) {
        $scope.$apply(function () {
            $scope.notifications.push(notification);
            alert(notification)
        });
    });
	
	$scope.sortType = 'title';
	$scope.sortReverse = false;
	$scope.searchBook = '';
	$scope.currentPage = 0;
	$scope.purchaseQuantity;
	$scope.pageSize = 12;
	$scope.allBooks = [];
	$scope.customersCart = [];
	$scope.price = 0;
	$scope.customersWithReviews = [];
	$scope.activeBookToReview = {};
	$scope.formReview = {};
	$scope.allReviews = [];
	$scope.currBookToAddOrDelete;
	$scope.addressObj = {};
	

	$scope.numberOfPages = function() {
		return Math.ceil($scope.allBooks.length / $scope.pageSize);
	};
	
	//Iterator Pattern
	var Iterator = function(items) {
		this.index = 0;
		this.items = items;
	}
	
	Iterator.prototype = {
		first: function() { 
			this.reset(); 
			return this.next(); 
		}, 
	    next: function() { 
	    	return this.items[this.index++]; 
	    }, 
	    hasNext: function() { 
	    	return this.index <= this.items.length; 
	    }, 
	    reset: function() { 
	    	this.index = 0; 
	    }, 
	    each: function(callback) { 
			for (var item = this.first(); this.hasNext(); item = this.next()) { 
			     callback(item); 
			} 
	    } 
	}

	var ShipAddress = function(shipAddress, shipCity, shipCounty, shipCountry) {
		this.shipAddress = shipAddress;
		this.shipCity = shipCity;
		this.shipCounty = shipCounty;
		this.shipCountry = shipCountry;
	}
	
	ShipAddress.prototype = {
			hydrate: function() {
				var memento = JSON.stringify(this);
				return memento;
			},
			dehydrate: function(memento) {
				var mem = JSON.parse(memento);
				this.shipAddress = mem.shipAddress;
				this.shipCity = mem.shipCity;
				this.shipCounty = mem.shipCounty;
				this.shipCountry = mem.shipCountry;
			}
	}

	$scope.Caretaker = function() {
		this.mementos = {};
		
		this.add = function(key, memento) {
			this.mementos[key] = memento;
		}
		
		this.get = function(key) {
	        return this.mementos[key];
		}
	}
	
	$scope.caretaker = new $scope.Caretaker();

	
	$scope.populateAddressFields = function() {
		$http.get('http://localhost:8080/bookstore/api/getCustomerByUsername/'+$scope.currentUser).success(function(customer) {
			$scope.addressObj.address = customer.address;
			$scope.addressObj.city = customer.city;
			$scope.addressObj.county = customer.county;
			$scope.addressObj.country = customer.country;
			$scope.add1 = new ShipAddress(customer.address, customer.city, customer.county, customer.country);
			$scope.caretaker.add(1, $scope.add1.hydrate());
		});
	}

	$scope.undoChanges = function() {
		$scope.add1.dehydrate($scope.caretaker.get(1));
		$scope.addressObj.address = $scope.add1.shipAddress;
		$scope.addressObj.city = $scope.add1.shipCity;
		$scope.addressObj.county = $scope.add1.shipCounty;
		$scope.addressObj.country = $scope.add1.shipCountry;
	}
	
	$scope.setCartInformation= function() {
		$http.get('http://localhost:8080/bookstore/api/getShoppingCart/'+$scope.currentUser).success(function(cart) {
			cart.items.forEach(function(cartItem) {
				cartItem.inCart = 'true';
				cartItem.book.quantityInCart = cartItem.quantity;
				cartItem.book.overallPrice = cartItem.quantity * cartItem.book.bookPrice;
				$scope.price = $scope.price + (cartItem.quantity * cartItem.book.bookPrice);
				$scope.customersCart.push(cartItem.book)
				console.log(cart)
			});
			$scope.setBooks();
		});
	}
	
	$scope.submitReview = function() {
		console.log($scope.activeBookToReview)
		console.log($scope.formReview.comment)
		console.log($scope.formReview.rating)
		var objToSend = {};
		objToSend.bookID = $scope.activeBookToReview.id;
		objToSend.username = $scope.currentUser;
		objToSend.rating = $scope.formReview.rating;
		objToSend.comment = $scope.formReview.comment;
		objToSend.username = $scope.currentUser;
		
		$http.post('http://localhost:8080/bookstore/api/createReview', JSON.stringify(objToSend))
        .success(function (data, status, headers, config) {
        	if(status == 200) {
        		console.log("Redirect to a new page");
        	}
        })
		.error(function(error) {
			alert("An error has occurred")
		});
	};
	
	$scope.purchaseClicked = function() {
		var objToSend = {};
		objToSend.address = $scope.addressObj.address;
		objToSend.city = $scope.addressObj.city;
		objToSend.county = $scope.addressObj.county;
		objToSend.country = $scope.addressObj.country;
		objToSend.username = $scope.currentUser;
		$http.post('http://localhost:8080/bookstore/api/purchaseClicked', JSON.stringify(objToSend))
        .success(function (data, status, headers, config) {
        	if(status == 200) {
        		$scope.customersCart = [];
        		$scope.price = 0;
        		console.log("Redirect to a new page");
        	}
        })
		.error(function(error) {
			alert("An error has occurred")
		});
	}
	
	$scope.setBooks = function() {
		$http.get('http://localhost:8080/bookstore/api/getAllBooks').success(function(books) {
			var iter = new Iterator(books);
			for (var item = iter.first(); iter.hasNext(); item = iter.next()) { 
				item.inCart = 'false';
				var iter2 = new Iterator($scope.customersCart);
				for (var item2 = iter2.first(); iter2.hasNext(); item2 = iter2.next()) { 
					if(item2.id == item2.id) {
						item2.inCart = 'false';
						item.inCart = 'false';
						item.quantityInCart = item2.quantityInCart;
						item.bookQuantity = item2.bookQuantity;
					}
			    }
				$scope.allBooks.push(item)
		    }
		});
	}	

	$http.get('http://localhost:8080/bookstore/api/getAllReviews').success(function(reviews) {
		$scope.allReviews = reviews;
	});
	
	$scope.addToCart = function(book) {
		book.inCart = 'true';
		book.bookQuantity = book.bookQuantity - book.purchaseQuantity;
		book.quantityInCart = book.purchaseQuantity;
		$scope.customersCart.push(book);
		book.overallPrice = book.bookPrice * book.quantityInCart;
		$scope.price = $scope.price + (book.bookPrice * book.quantityInCart);
		book.purchaseQuantity = 0;
		console.log(book)
		var objToSend = {};
		objToSend.username = $scope.currentUser;
		objToSend.price = $scope.price;
		objToSend.bookId = book.id;
		objToSend.bookQuantity = book.quantityInCart;
		
		$http.post('http://localhost:8080/bookstore/api/saveCart', JSON.stringify(objToSend))
			.success(function (data, status, headers, config) {
				if(status == 200) {
					console.log("Redirect to a new page");
				}
        	})
			.error(function(error) {
				alert("An error has occurred")
			});
		}
	
	$scope.plusClicked = function(item) {
		if(item.bookQuantity != 0) {
			item.quantityInCart = item.quantityInCart +1;
			$scope.allBooks.forEach(function(b) {
				if(b.bookQuantity != 0) {
					if(b.id == item.id) {
						item.overallPrice = item.overallPrice + b.bookPrice;
						b.bookQuantity = b.bookQuantity - 1;
						var changesToSend = {};
						changesToSend.username = $scope.currentUser;
						changesToSend.id = b.id;
						changesToSend.bookprice = b.bookPrice;
						$http.post('http://localhost:8080/bookstore/api/plusButtonPressed', JSON.stringify(changesToSend))
					      .success(function (data, status, headers, config) {
					      	if(status == 200) {
					      		console.log("Redirect to a new page");
					      	}
				        })
					}
				}
			})
		} else {
			alert("No Books Available");
		}
	}
	
	$scope.removeFromCart = function(itemToRemove) {
		itemToRemove.bookQuantity = itemToRemove.bookQuantity + itemToRemove.quantityInCart;
		var i = $scope.customersCart.indexOf(itemToRemove);
		if(i != -1) {
			$scope.customersCart.splice(i, 1);
		}
		$scope.price = $scope.price - (itemToRemove.bookPrice*itemToRemove.quantityInCart);
		itemToRemove.quantityInCart = 0;
		itemToRemove.inCart = 'false';
	}
	
	$scope.minusClicked = function(item) {
		item.quantityInCart = item.quantityInCart -1;
		if(item.quantityInCart > 0) {
			$scope.allBooks.forEach(function(b) {
				if(b.id == item.id) {
					console.log(item)
					console.log(b)
					item.overallPrice = item.overallPrice - b.bookPrice;
					b.bookQuantity = b.bookQuantity + 1;
					var changesToSend = {};
					changesToSend.username = $scope.currentUser;
					changesToSend.id = b.id;
					changesToSend.bookprice = b.bookPrice;
					$scope.price = $scope.price - b.bookPrice;
					$http.post('http://localhost:8080/bookstore/api/minusButtonPressed', JSON.stringify(changesToSend))
				      .success(function (data, status, headers, config) {
				      	if(status == 200) {
				      	}
			        })
				}
			})
		} else if(item.quantityInCart == 0) {
			$scope.allBooks.forEach(function(b) {
				if(b.id == item.id) {
					b.inCart = 'false';
					item.inCart = 'false';
					b.bookQuantity = b.bookQuantity + 1;
					var changesToSend = {};
					changesToSend.username = $scope.currentUser;
					changesToSend.id = b.id;
					changesToSend.bookprice = b.bookPrice;
					$scope.price = $scope.price - b.bookPrice;
					$http.post('http://localhost:8080/bookstore/api/deleteCartItem', JSON.stringify(changesToSend))
				      .success(function (data, status, headers, config) {
				      	if(status == 200) {
				      	}
			        })
			        $scope.removeFromCart(item);
				}
			});
		}
	};
	
	$scope.clearCart = function() {
		var changesToSend = $scope.currentUser;
		$http.post('http://localhost:8080/bookstore/api/clearCart', JSON.stringify(changesToSend))
	      .success(function (data, status, headers, config) {
	      	if(status == 200) {
	      		console.log("SUCCESS")
	      	}
        });
		
		$scope.allBooks.forEach(function(book) {
			$scope.customersCart.forEach(function(cartItem) {
				if(book.id == cartItem.id) {
					cartItem.inCart = 'false';
					book.inCart = 'false';
					console.log(book);
					console.log(cartItem);
					cartItem.bookQuantity = cartItem.bookQuantity + cartItem.quantityInCart;
				}
			})
		});
		
		$scope.customersCart = [];
		$scope.price = 0.00;
	}
	
	$scope.reviewsFunctionality = function(book) {
		$scope.allReviews = [];
		$scope.activeBookToReview = book;
		if($scope.activeBookToReview.reviews.length != 0) {
			$scope.activeBookToReview.reviews.forEach(function(review) {
				var reviewForBook = {};
				reviewForBook.comment = review.comment;
				reviewForBook.rating = review.rating;
				reviewForBook.title = book.bookTitle;
				$scope.allReviews.push(reviewForBook);
			});
		}
	}
}]);

//My Filter for the search bar
viewAllBooks.filter('startFrom', function() {
	return function(input, start) {
		start = +start;
		return input.slice(start);
	}
});
