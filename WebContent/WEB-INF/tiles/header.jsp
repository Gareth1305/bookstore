<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script src="${pageContext.request.contextPath}/static/js/angular.min.js"></script>

<nav class="navbar navbar-inverse navbar-fixed-top">
	<sec:authentication property="principal" var="principal" />
      <div class="container">
        <div class="navbar-header">                  
          <a class="project_title navbar-brand" href="${pageContext.request.contextPath}/">Gareth's Book Store</a>
          <sec:authorize access="hasRole('ROLE_CUSTOMER')">
          	<a class="navbar-brand" href="${pageContext.request.contextPath}/books">Search Books</a>
       	  </sec:authorize>
       	  <sec:authorize access="hasRole('ROLE_CUSTOMER')">
          	<a class="navbar-brand" href="${pageContext.request.contextPath}/viewcustomersorders">View Orders</a>
       	  </sec:authorize>
       	  <sec:authorize access="hasRole('ROLE_ADMIN')">
          	<a class="navbar-brand" href="${pageContext.request.contextPath}/booksadminview">View Books</a>
       	  </sec:authorize>
       	  <sec:authorize access="hasRole('ROLE_ADMIN')">
          	<a class="navbar-brand" href="${pageContext.request.contextPath}/broadcastmessage">Broadcast Message</a>
       	  </sec:authorize>
       	  <sec:authorize access="hasRole('ROLE_ADMIN')">
          	<a class="navbar-brand" href="${pageContext.request.contextPath}/createbook">Create Books</a>
       	  </sec:authorize>
       	  <sec:authorize access="hasRole('ROLE_ADMIN')">
          	<a class="navbar-brand" href="${pageContext.request.contextPath}/customerswithpurchases">Customers With Orders</a>
       	  </sec:authorize>
		  <sec:authorize access="!isAuthenticated()">
			 <a class="navbar-brand" href="${pageContext.request.contextPath}/newaccount">Create Account</a>
		  </sec:authorize>
		  <sec:authorize access="hasRole('ROLE_ADMIN')">
			 <a class="navbar-brand" href="${pageContext.request.contextPath}/createadminaccount">Create Admin</a>
		  </sec:authorize>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
	        <sec:authorize access="!isAuthenticated()">
	          <form class="navbar-form navbar-right"
	          		name='f'
					action='${pageContext.request.contextPath}/j_spring_security_check'
					method='POST'>
	            <div class="form-group">
	              <input type="text" placeholder="Username" name='j_username' value='' class="form-control">
	            </div>
	            <div class="form-group">
	              <input type="password" name='j_password' placeholder="Password" class="form-control">
	            </div>
	            <input type='checkbox' name=_spring_security_remember_me
				checked="checked" />
	            <button type="submit" class="btn btn-success">Sign in</button>
	          </form>
            </sec:authorize>	
            <sec:authorize access="isAuthenticated()">
				<a class="login" href="${pageContext.request.contextPath}/j_spring_security_logout"><button class="btn btn-success">Log Out</button></a>
			</sec:authorize>	
        </div>
      </div>
    </nav>
    
    