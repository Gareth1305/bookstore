<script
	src="${pageContext.request.contextPath}/static/js/angular.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/newaccount/newaccount.js"></script>

<div class="formDiv" ng-app="newAccountPage"
	ng-controller="NewAccountCtrl">
	<form ng-submit="onSubmit()" novalidate="novalidate"
		ng-hide="submitted" ng-model="userame">
		<h2>Create Account</h2>
		<div class="split-form-container">
			<div class="first-split-form">
				<div class="form-group form-group_2">
					<label for="customername" class="control-label">Username</label> <input
						type="text" class="form-control form-control_2"
						ng-model="formModel.customername" id="customername"
						required="required">
				</div>
				<div class="form-group form-group_2">
					<label for="email" class="control-label">Email</label> <input
						type="email" class="form-control form-control_2"
						ng-model="formModel.email" id="email" required="required">
				</div>
				<div class="form-group form-group_2">
					<label for="password" class="control-label">Password</label> <input
						type="password" class="form-control form-control_2"
						ng-model="formModel.password" id="password" required="required">
				</div>
				<div class="form-group form-group_2 confirm_pass_div">
					<label for="confirmpass" class="control-label">Confirm
						Password</label> <input type="password"
						class="form-control form-control_2"
						ng-model="formModel.confirmpass" id="confirmpass"
						required="required">
					<div id="matchpass"></div>
				</div>
			</div>
			<div class="second-split-form">
				<div class="form-group form-group_2">
					<label for="address" class="control-label">Address</label> 
					<input type="text" class="form-control form-control_2"
						ng-model="formModel.address" id="address"
						required="required">
				</div>
				<div class="form-group form-group_2">
					<label for="city" class="control-label">City</label> 
					<input type="text" class="form-control form-control_2"
						ng-model="formModel.city" id="city"
						required="required">
				</div>
				<div class="form-group form-group_2">
					<label for="county" class="control-label">County</label> 
					<input type="text" class="form-control form-control_2"
						ng-model="formModel.county" id="county"
						required="required">
				</div>
				<div class="form-group form-group_2">
					<label for="country" class="control-label">Country</label> 
					<input type="text" class="form-control form-control_2"
						ng-model="formModel.country" id="country"
						required="required">
				</div>
			</div>
		</div>
		<div class="form-group form-group_2 form-form-group submit-button-div">
			<button id="subBtn"
			 class="btn btn-primary submit-button" type="submit">Create
				Account</button>
		</div>
	</form>
</div>
<script
	src="${pageContext.request.contextPath}/static/js/jcs-auto-validate.min.js"></script>