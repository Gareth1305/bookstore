<script
	src="${pageContext.request.contextPath}/static/js/angular.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/createadminaccount/createadminaccount.js"></script>

<div class="formDiv" ng-app="newAdminAccountPage"
	ng-controller="NewAdminAccountCtrl">
	<form ng-submit="onSubmit()" novalidate="novalidate"
		ng-hide="submitted" ng-model="userame">
		<h2>Create Admin Account</h2>
		<div class="split-form-container">
			<div class="form-group form-group_2">
				<label for="adminname" class="control-label">Username</label> <input
					type="text" class="form-control form-control_2"
					ng-model="formModel.adminname" id="adminname"
					required="required">
			</div>
			<div class="form-group form-group_2">
				<label for="email" class="control-label">Email</label> <input
					type="email" class="form-control form-control_2"
					ng-model="formModel.email" id="email" required="required">
			</div>
			<div class="form-group form-group_2">
				<label for="password" class="control-label">Password</label> <input
					type="password" class="form-control form-control_2"
					ng-model="formModel.password" id="password" required="required">
			</div>
			<div class="form-group form-group_2 confirm_pass_div">
				<label for="confirmpass" class="control-label">Confirm
					Password</label> <input type="password"
					class="form-control form-control_2"
					ng-model="formModel.confirmpass" id="confirmpass"
					required="required">
				<div id="matchpass"></div>
			</div>
		</div>
		<div class="form-group form-group_2 form-form-group submit-button-div">
			<button id="subBtn"
			 class="btn btn-primary submit-button" type="submit">Create
				Account</button>
		</div>
	</form>
</div>
<script
	src="${pageContext.request.contextPath}/static/js/jcs-auto-validate.min.js"></script>