<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<head>
<link
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet" media="screen" />

<script
	src="${pageContext.request.contextPath}/static/js/books/viewbooks.js"></script>

<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.1/js/jasny-bootstrap.js"></script>
<link
	href="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.1/css/jasny-bootstrap.css"
	rel="stylesheet" media="screen" />
<link
	href="${pageContext.request.contextPath}/static/css/bootstrap-theme.min.css"
	rel="stylesheet" media="screen" />

<style>
body {
	min-height: 100vh;
}

.divContainer {
	height: 90%;
	min-height: 90.5vh;
	position: absolute;
	overflow: hidden;
	padding-left: 5px;
	padding-right: 5px;
	padding-top: 5px;
	width: 100%;
}

.orderContainer {
	position: relative;
	border: 1px solid black;
	width: 20%;
	height: 100%;
	overflow-y: hidden;
	overflow-x: hidden;
}

.contentContainer {
	width: 80%;
	float: left;
	height: 100%;
	overflow-y: auto;
	overflow-x: hidden;
}

.leftTD {
	width: 60%;
	float: left;
	height: 150px;
	overflow-y: hidden;
	overflow-x: hidden;
	text-align: center;
	vertical-align: middle;
	top: 50%;
	padding-top: 90px;
}

.tableCell {
	text-align: center;
	vertical-align: middle;
}

.rightTD {
	width: 40%;
	height: 150px;
	overflow-y: hidden;
	overflow-x: hidden;
}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th,
	.table>thead>tr>td, .table>thead>tr>th {
	padding: 8px;
	line-height: 1.42857143;
	vertical-align: middle;
	text-align: center;
	border-top: 1px solid #ddd;
}

.titleTD {
	width: 30%;
}

.authorTD {
	width: 20%;
}

.genreTD {
	width: 20%;
}

.priceTD {
	width: 10%;
}

.conditionTD {
	width: 10%;
}

.addToCartColumn {
	color: #337ab7;
	width: 10%;
}

.reviewsColumn {
	color: #337ab7;
	width: 10%;
}

.cart_content_div {
	height: 85%;
	padding-top: 10px;
}

.cart_title_div {
	text-decoration: underline;
	padding-top: 10px;
	text-align: center;
	height: 8%;
	font-size: 22px;
}

.itemDiv {
	margin-bottom: left;
	margin-left: 5px;
	margin-right: 5px;
}

.removeItem {
	color: #337ab7;
}

.priceDiv {
	height: 7%;
	bottom: 0;
}

.cartInfoDiv {
	border-top: 1px solid black;
	border-right: 1px solid black;
	float: left;
	height: 100%;
	width: 70%;
}

.purchaseDiv {
	height: 100%;
	width: 30%;
	margin-left: 70%;
}

.purchaseButton {
	background-color: #4CAF50;
	border-top: 1px solid black;
	border-left: none;
	border-right: none;
	border-bottom: none;
	color: white;
	height: 100%;
	width: 100%;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
	cursor: pointer;
}

.overflowPageDiv {
	vertical-align: middle;
	text-align: center;
}

.switchPageBtn {
	width: 80px;
	height: 25px;
}

.purchaseButton:disabled {
	cursor: not-allowed;
}

.first_modal_dialog {
	width: 60%
}

.textarea_input {
	height: 113px;
}

.giveMeMargin {
	margin-left: 10px;
}
</style>
</head>
<sec:authentication property="principal" var="principal" />
<div class="divContainer" ng-app="viewAllBooks"
	ng-controller="ViewAllBooksCtrl" ng-model="currentUser"
	ng-init="currentUser='${principal.username}'">
	<div class="contentContainer" ng-init="setCartInformation()">
		<form class="search_form">
			<div class="form-group">
				<div class="input-group">
					<div class="input-group-addon">
						<i class="fa fa-search"></i>
					</div>
					<input type="text" class="form-control" placeholder="Search Books"
						ng-model="searchBook">
				</div>
			</div>
		</form>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<td class="tableCell titleTD"><a href="#"
						ng-click="sortType = 'bookTitle'; sortReverse = !sortReverse">Title:
							<span ng-show="sortType == 'bookTitle' && !sortReverse"
							class="fa fa-caret-down"></span> <span
							ng-show="sortType == 'bookTitle' && sortReverse"
							class="fa fa-caret-up"></span>
					</a></td>
					<td class="tableCell authorTD"><a href="#"
						ng-click="sortType = 'bookAuthor'; sortReverse = !sortReverse">Author:
							<span ng-show="sortType == 'bookAuthor' && !sortReverse"
							class="fa fa-caret-down"></span> <span
							ng-show="sortType == 'bookAuthor' && sortReverse"
							class="fa fa-caret-up"></span>
					</a></td>
					<td class="tableCell genreTD"><a href="#"
						ng-click="sortType = 'bookGenre'; sortReverse = !sortReverse">Genre:
							<span ng-show="sortType == 'bookGenre' && !sortReverse"
							class="fa fa-caret-down"></span> <span
							ng-show="sortType == 'bookGenre' && sortReverse"
							class="fa fa-caret-up"></span>
					</a></td>
					<td class="tableCell priceTD"><a href="#"
						ng-click="sortType = 'bookPrice'; sortReverse = !sortReverse">Price:
							<span ng-show="sortType == 'bookPrice' && !sortReverse"
							class="fa fa-caret-down"></span> <span
							ng-show="sortType == 'bookPrice' && sortReverse"
							class="fa fa-caret-up"></span>
					</a></td>
					<td class="tableCell conditionTD"><a href="#"
						ng-click="sortType = 'bookCondition'; sortReverse = !sortReverse">In
							Stock: <span
							ng-show="sortType == 'bookCondition' && !sortReverse"
							class="fa fa-caret-down"></span> <span
							ng-show="sortType == 'bookCondition' && sortReverse"
							class="fa fa-caret-up"></span>
					</a></td>
					<td class="tableCell conditionTD"><a href="#"
						ng-click="sortType = 'bookCondition'; sortReverse = !sortReverse">Purchase
							Quantity: </a></td>
					<td class="tableCell addToCartColumn" align="center" width="137px">Add
						To Cart</td>
					<td class="tableCell reviewsColumn" align="center" width="137px">Reviews</td>
				</tr>
			</thead>
			<tbody>
				<tr
					ng-repeat="book in allBooks | orderBy:sortType:sortReverse | filter:searchBook | startFrom:currentPage*pageSize | limitTo:pageSize">
					<td>
						<div class="leftTD">{{ book.bookTitle }}</div>
						<div class="rightTD">
							<img
								src="${pageContext.request.contextPath}/static/images/{{book.bookImageTitle}}"
								width="100%" height="100%">
						</div>
					</td>
					<td>{{ book.bookAuthor }}</td>
					<td>{{ book.bookGenre }}</td>
					<td>&#8364;{{ book.bookPrice.toFixed(2) }}</td>
					<td>{{ book.bookQuantity }}</td>
					<td align="center"><input ng-disabled="book.inCart=='true'"
						type="number" min="0" max="{{book.bookQuantity}}"
						class="form-control" ng-model="book.purchaseQuantity"
						ng-init="book.purchaseQuantity=0" id="purchaseQuantity"
						required="required"></td>
					<td ng-if="book.inCart == 'true'" align="center" class="btn_cell"><button
							class="btn btn-default" disabled>In Cart</button></td>
					<td ng-if="book.inCart == 'false'" align="center" class="btn_cell"><button
							class="btn btn-primary" ng-click="addToCart(book)"
							ng-disabled="book.bookQuantity==0 || book.purchaseQuantity>book.bookQuantity || book.purchaseQuantity==0">Add</button></td>
					<td align="center" class="btn_cell"><button
							ng-click="reviewsFunctionality(book)" class="btn btn-primary"
							data-toggle="modal" data-target="#myModal">Reviews</button></td>
				</tr>
			</tbody>
		</table>
		<div class="overflowPageDiv">
			<button class="switchPageBtn" ng-disabled="currentPage == 0"
				ng-click="currentPage=currentPage-1">Previous</button>
			{{currentPage+1}}/{{numberOfPages()}}
			<button class="switchPageBtn"
				ng-disabled="currentPage >= data.length/pageSize - 1"
				ng-click="currentPage=currentPage+1">Next</button>
		</div>
	</div>
	<div class="orderContainer">
		<div class="cart_title_div">
			Shopping Cart<a ng-if="customersCart.length != 0" href="#"
				ng-click="clearCart()"> - Clear</a>
		</div>
		<div class="cart_content_div">
			<div class="itemDiv" ng-repeat="item in customersCart">
				<div>
					<b>Title:</b> {{item.bookTitle}}
				</div>
				<div>
					<b>Author:</b> {{item.bookAuthor}}
				</div>
				<div>
					<b>Price:</b> &#8364;{{item.bookPrice.toFixed(2)}}
				</div>
				<div>
					<b>Quantity:</b> {{item.quantityInCart}} <a class="giveMeMargin"
						href="#" ng-click="minusClicked(item)"> <font size="4">--</font>
					</a> / <a href="#" ng-click="plusClicked(item)"> <font size="4">+</font>
					</a>
				</div>
				<div>
					<b>Overall Price:</b> &#8364;{{item.overallPrice.toFixed(2)}}
				</div>
				<!--<div class="removeItem"><a ng-href="#" ng-click="removeFromCart(item)">Remove From Cart</a></div>-->
				<hr />
			</div>
		</div>
		<div class="priceDiv">
			<div class="cartInfoDiv">
				<div>Different Books: {{customersCart.length}}</div>
				<div>Price: &#8364;{{price.toFixed(2)}}</div>
			</div>
			<div class="purchaseDiv">
				<button ng-disabled="customersCart.length == 0" type="button"
					class="purchaseButton" ng-init="populateAddressFields()" data-toggle="modal"
						data-target="#myModal3">Purchase</button>
			</div>
		</div>
	</div>

	<div id="myModal" class="modal fade">
		<div class="first_modal_dialog modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Book Reviews</h4>
				</div>
				<div class="modal-body clearfix">
					<div ng-if="allReviews.length != 0">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<td class="tableCell reviewerTD">Book Title:</td>
									<td class="tableCell ratingTD">Rating:</td>
									<td class="tableCell commentTD">Comment:</td>
								</tr>
							</thead>
							<tbody>
								<tr
									ng-repeat="review in allReviews | orderBy:sortType:sortReverse | filter:searchBook | startFrom:currentPage*pageSize | limitTo:pageSize">
									<td>{{ review.title }}</td>
									<td>{{ review.rating }}</td>
									<td>{{ review.comment }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div ng-if="allReviews.length == 0">There are no reviews for
						this book yet!</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#myModal2" data-dismiss="modal">Leave Review</button>
				</div>
			</div>
		</div>
	</div>
	<div id="myModal2" class="modal fade">
		<div class="first_modal_dialog modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Second Model</h4>
				</div>
				<div class="modal-body clearfix">
					<form name='form' ng-submit="onSubmit()" novalidate="novalidate">
						<div class="modalEditableDiv">
							<div class="split-form-container">
								<div class="first-split-form">
									<div class="form-group form-group_2">
										<label for="id" class="control-label">Book ID:</label> <input
											type="text" class="form-control form-control_2"
											ng-model="activeBookToReview.id" id="id" disabled
											required="required">
									</div>
									<div class="form-group form-group_2">
										<label for="bookTitle" class="control-label">Title:</label> <input
											type="text" name="" class="form-control form-control_2"
											ng-model="activeBookToReview.bookTitle" disabled
											id="bookTitle" required="required">
									</div>
									<div class="form-group form-group_2">
										<label for="bookAuthor" class="control-label">Username:</label>
										<input type="text" class="form-control form-control_2"
											disabled ng-model="currentUser" id="bookAuthor"
											required="required">
									</div>
								</div>
								<div class="second-split-form">
									<div class="form-group form-group_2">
										<label for="rating" class="control-label">Rating : </label> <input
											type="number" min="1" max="5"
											class="form-control form-control_2"
											ng-model="formReview.rating" id="rating"
											ng-disabled="formFieldsActive" required="required">
									</div>
									<div class="form-group form-group_2">
										<label for="comment" class="control-label">Comment: </label>
										<textarea style="height: 113px;" type="text"
											class="textarea_input form-control form-control_2"
											ng-model="formReview.comment" id="comment"
											required="required" ng-disabled="formFieldsActive"></textarea>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary"
						ng-click="submitReview()" ng-disabled="!form.$dirty"
						data-dismiss="modal">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	<div id="myModal3" class="modal fade">
		<div class="first_modal_dialog modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Enter Shipping Address</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="address">Enter Address:</label> <input type="text"
							class="form-control" ng-model="addressObj.address" id="address">
					</div>
					<div class="form-group">
						<label for="city">Enter City:</label> <input type="text"
							class="form-control" ng-model="addressObj.city" id="city">
					</div>
					<div class="form-group">
						<label for="county">Enter County:</label> <input type="text"
							class="form-control" ng-model="addressObj.county" id="county">
					</div>
					<div class="form-group">
						<label for="country">Enter Country:</label> <input type="text"
							class="form-control" ng-model="addressObj.country" id="country">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-default" ng-click="undoChanges()">Undo Changes</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="purchaseClicked()">Confirm Address</button>
				</div>
			</div>
		</div>
	</div>
</div>