<head>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="screen"/> 

<script src="${pageContext.request.contextPath}/static/js/viewallcustomers/viewallcustomers.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.1/js/jasny-bootstrap.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.1/css/jasny-bootstrap.css" rel="stylesheet" media="screen"/> 
<link href="${pageContext.request.contextPath}/static/css/bootstrap-theme.min.css" rel="stylesheet" media="screen"/> 
<style>
.viewOrdersTD {
	color: #337ab7;
	width:15%;
}
.usernameTD {
	color: #337ab7;
	width:20%;
}
.emailTD {
	color: #337ab7;
	width:20%;
}
.shipTD {
	color: #337ab7;
	width:45%;
}
.table {
    background-color: white;
}
.contentContainer {
	margin-top: 15px;
	margin-left:10px;
	margin-right:10px;
}
.tableCell {
	text-align: center;
	vertical-align: middle;
}
.first_modal_dialog {
	width:60%
}
</style>

</head>

<div class="divContainer" ng-app="viewAllCustomersPage" ng-controller="ViewAllCustomersCtrl">

	<div class="contentContainer">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<td class="tableCell usernameTD" align="center">Username:</td>
					<td class="tableCell emailTD" align="center">Email:</td>
					<td class="tableCell shipTD" align="center">Shipping Address:</td>
					<td class="tableCell viewOrdersTD" align="center">View Orders</td>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="cust in customers">
			    	<td align="center">{{cust.username}}</td>
			    	<td align="center">{{cust.email}}</td>
			        <td align="center">{{cust.address}}, {{cust.city}}, {{cust.county}}, {{cust.country}}</td>
			        <td align="center" class="btn_cell">
			        	<button ng-if="cust.orders.length!=0" ng-click="viewOrders(cust)" data-toggle="modal" data-target="#myModal"style="width:100px; height:33.09px;" class="btn btn-primary">View Orders</button>
			        	<button ng-if="cust.orders.length==0" disabled class="btn btn-default" style="width:100px; height:33.09px;">No Orders</button>
			        </td>
				</tr>
			</tbody>
		</table>
	</div>
	<div id="myModal" class="modal fade">
	    <div class="first_modal_dialog modal-dialog modal-sm">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">{{activeCustomer.username}}'s Orders</h4>
	            </div>
	            <div class="modal-body">
	                <div class="orderDiv" ng-repeat="order in activeCustomer.orders">
						<div>Order ID: {{order.id}}</div>
						<div>Order Cost: &#8364;{{order.orderCost.toFixed(2)}}</div>
						<div>Books Ordered:
							<div class="individualBookDiv">
								<table class="table">
									<tr>
									    <th class="table-cell">Title</th>
									    <th class="table-cell">Author</th>		
									    <th class="table-cell">Genre</th>
									    <th class="table-cell">Price</th>
								  	</tr>	
									<tr ng-repeat="book in order.books">
				    					<td class="table-cell">{{book.bookTitle}}</td>
				    					<td class="table-cell">{{book.bookAuthor}}</td>		
				    					<td class="table-cell">{{book.bookGenre}}</td>
				    					<td class="table-cell">&#8364;{{book.bookPrice.toFixed(2)}}</td>
								  	</tr>
								</table>
							</div>
						</div>
					</div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>