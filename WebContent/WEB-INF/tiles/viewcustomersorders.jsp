<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<head>
	<script src="${pageContext.request.contextPath}/static/js/viewcustomersorders/viewcustomersorders.js"></script>

	<style>
		.divRepeaterContainer{
			margin:15px;
		}
		.orderDiv{
			margin-bottom:15px;
			background-color: #fff;
		}
		.individualBookDiv {
			padding-left:20px;
		}
		.pageTitle {
			margin-left:15px;
			margin-right:15px;
		}
		table, th, td {
		    border-collapse: collapse;
		}
		th, td {
		    padding: 5px;
		}
		.table-cell {
			width:25%;
		}
	</style>
</head>

<sec:authentication property="principal" var="principal" />
<div class="divContainer" ng-app="viewCustomersOrders" ng-controller="ViewCustomersOrdersCtrl" ng-model="custUsername" ng-init="custUsername='${principal.username}'">
	<h2 class="pageTitle" ng-init="loadData()"><u>View Your Orders</u></h2>
	
	<div ng-if="customersOrders.length != 0" class="divRepeaterContainer">
		<div class="orderDiv" ng-repeat="order in customersOrders">
			<div>Order ID: {{order.id}}</div>
			<div>Order Cost: &#8364;{{order.cost.toFixed(2)}}</div>
			<div>Books Ordered:
				<div class="individualBookDiv">
					<table class="table">
						<tr>
						    <th class="table-cell">Title</th>
						    <th class="table-cell">Author</th>		
						    <th class="table-cell">Genre</th>
						    <th class="table-cell">Price</th>
					  	</tr>	
						<tr ng-repeat="book in order.phLineItems">
	    					<td class="table-cell">{{book.book.bookTitle}} x{{book.quantity}}</td>
	    					<td class="table-cell">{{book.book.bookAuthor}}</td>		
	    					<td class="table-cell">{{book.book.bookGenre}}</td>
	    					<td class="table-cell">&#8364;{{book.book.bookPrice.toFixed(2)}}</td>
					  	</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div ng-if="customersOrders.length == 0" class="divRepeaterContainer">
		<h3>You do not have any orders!</h3>
	</div>
	
</div>