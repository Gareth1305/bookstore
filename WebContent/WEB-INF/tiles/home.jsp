<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
	<div class="container">
		<h1>Welcome To Gareth's Book Store</h1>
		<p>We have plenty of books to choose from. Search through them,
			purchase one and we will have it at your door within 24 hrs</p>
		<p>
			<a class="btn btn-primary btn-lg" href="#" role="button">Learn
				more &raquo;</a>
		</p>
	</div>
</div>

<div class="container">
	<!-- Example row of columns -->
	<div class="row">
		<div class="col-md-4">
			<h2>Searching</h2>
			<p>This website will allow you to search through an array of your
				favourite books. There are many books in almost every category one
				could think of. Through this website you can purchase your favourite
				books and have them delivered to your doorstep. We guarantee 100%
				customer satisfaction</p>
			<p>
				<a class="btn btn-default" href="#" role="button">View details
					&raquo;</a>
			</p>
		</div>
		<div class="col-md-4">
			<h2>Weekly Favourites</h2>
			<ul>
				<li>Patterns of Enterprise Application Architecture</li>
				<li>Design Patterns For Dummies</li>
				<li>Head First Design Patterns</li>
				<li>Design Patterns: Elements of Reusable Object-Oriented
					Software</li>
			</ul>
			<p>
				<a class="btn btn-default" href="#" role="button">View details
					&raquo;</a>
			</p>
		</div>
		<div class="col-md-4">
			<h2>Special Offers</h2>
			<ul>
				<li>Java: A Beginner's Guide - &#8364;9.99</li>
				<li>Core Java Volume 1 - &#8364;7.95</li>
				<li>Java For Dummies - &#8364;14.99</li>
				<li>Data Structures and Algorithms in Java - &#8364;20.00</li>
			</ul>
			<p>
				<a class="btn btn-default" href="#" role="button">View details
					&raquo;</a>
			</p>
		</div>
	</div>
</div>