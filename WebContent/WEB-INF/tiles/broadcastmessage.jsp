<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<script
	src="${pageContext.request.contextPath}/static/js/header/header.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.1/js/jasny-bootstrap.js"></script>
<link
	href="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.1/css/jasny-bootstrap.css"
	rel="stylesheet" media="screen" />
<link
	href="${pageContext.request.contextPath}/static/css/bootstrap-theme.min.css"
	rel="stylesheet" media="screen" />

<sec:authentication property="principal" var="principal" />
<div ng-app="broadcastPage" ng-controller="broadcastCtrl"
	class="container" ng-model="currentUser"
	ng-init="currentUser='${principal.username}'">
	<h2>Broadcast a Message</h2>
	<form role="form">
		<div class="form-group">
			<label for="msg">Message:</label> <input type="text"
				class="form-control" ng-model="bMessage" id="msg">
		</div>
		<button ng-disabled="bMessage==''" type="button" ng-click="submit()"
			class="btn btn-primary" type="submit">Broadcast</button>
	</form>
</div>