<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript">
	function onLoad() {
		document.getElementById("subBtn").disabled = true;
		$("#password").keyup(checkPasswordsMatch);
		$("#confirmpass").keyup(checkPasswordsMatch);
		$("#details").submit(canSubmit);
	}

	function canSubmit() {
		var password = $("#password").val();
		var confirmpass = $("#confirmpass").val();

		if (password != confirmpass) {
			alert("Passwords do not match")
			return false;
		} else {
			return true;
		}
	}

	function checkPasswordsMatch() {
		var password = $("#password").val();
		var confirmpass = $("#confirmpass").val();

		if (password.length > 3 || confirmpass.length > 3) {

			if (password == confirmpass) {
				document.getElementById("subBtn").disabled = false;
				$("#matchpass").text("Passwords match");
				$("#matchpass").addClass("passvalid");
				$("#matchpass").removeClass("passerror");
			} else {
				document.getElementById("subBtn").disabled = true;
				$("#matchpass").text("Passwords do not match");
				$("#matchpass").addClass("passerror");
				$("#matchpass").removeClass("passvalid");
			}
		}
	}

	$(document).ready(onLoad);
</script>