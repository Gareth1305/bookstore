<script
	src="${pageContext.request.contextPath}/static/js/angular.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/createbook/createbook.js"></script>

<div class="formDiv" ng-app="newBooksPage"ng-controller="NewBooksCtrl">
	<form ng-submit="onSubmit()" novalidate="novalidate" ng-hide="submitted">
		<h2>Create Book</h2>
		<div class="split-form-container">
			<div class="first-split-form">
				<div class="form-group form-group_2">
					<label for="booktitle" class="control-label">Book Title</label> <input
						type="text" class="form-control form-control_2"
						ng-model="formModel.booktitle" id="booktitle"
						required="required">
				</div>
				<div class="form-group form-group_2">
					<label for="bookauthor" class="control-label">Book Author</label> <input
						type="text" class="form-control form-control_2"
						ng-model="formModel.bookauthor" id="bookauthor" required="required">
				</div>
				<div class="form-group form-group_2">
					<label for="bookgenre" class="control-label">Book Genre</label> <input
						type="text" class="form-control form-control_2"
						ng-model="formModel.bookgenre" id="bookgenre" required="required">
				</div>
			</div>
			<div class="second-split-form">
				<div class="form-group form-group_2">
					<label for="bookprice" class="control-label">Book Price</label> 
					<input type="text" class="form-control form-control_2"
						ng-model="formModel.bookprice" id="bookprice"
						required="required">
				</div>
				<div class="form-group form-group_2">
					<label for="bookimagetitle" class="control-label">Image Title</label> 
					<input type="text" class="form-control form-control_2"
						ng-model="formModel.bookimagetitle" id="bookimagetitle"
						required="required">
				</div>
				<div class="form-group form-group_2">
					<label for="bookquantity" class="control-label">Quantity</label> 
					<input type="text" class="form-control form-control_2"
						ng-model="formModel.bookquantity" id="bookquantity"
						required="required">
				</div>
			</div>
		</div>
		<div class="form-group form-group_2 form-form-group submit-button-div">
			<button id="subBtn"
			 class="btn btn-primary submit-button" type="submit">Create Book</button>
		</div>
	</form>
</div>
<script
	src="${pageContext.request.contextPath}/static/js/jcs-auto-validate.min.js"></script>