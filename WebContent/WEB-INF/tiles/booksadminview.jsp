<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<head>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="screen"/> 

<script src="${pageContext.request.contextPath}/static/js/booksadminview/booksadminview.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.1/js/jasny-bootstrap.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.1/css/jasny-bootstrap.css" rel="stylesheet" media="screen"/> 
<link href="${pageContext.request.contextPath}/static/css/bootstrap-theme.min.css" rel="stylesheet" media="screen"/> 

<style>
body {
	min-height: 100vh;
}
.divContainer {
	height: 90%;
	min-height: 90.5vh;
	position: absolute;
	overflow: hidden;
	padding-left:5px;
	padding-right:5px;
	padding-top:5px;
	width: 100%;
}
.contentContainer {
	width: 100%;
	height: 100%;
	overflow-y: auto;
	overflow-x: hidden;
}
.tableCell {
	text-align: center;
	vertical-align: middle;
}
.table {
    background-color: white;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: middle;
    text-align: center;
    border-top: 1px solid #ddd;
}
.titleTD {
	width:20%;
}
.authorTD {
	width:15%;
}
.genreTD {
	width:15%;
}
.priceTD {
	width:15%;
}
.conditionTD {
	width:15%;
}
.deleteBookColumn {
	color: #337ab7;
	width:10%;
}
.updateBookColumn {
	color: #337ab7;
	width:10%;
}
.overflowPageDiv {
	vertical-align: middle;
    text-align: center;
}
.switchPageBtn {
	width:80px;
	height:25px;
}
.modalPictureDiv {
	float:left;
	width:30%;
	height:100%;
}
.modalEditableDiv {
	width:70%;
	height:100%;
	margin-left:30%
}
.first_modal_dialog {
	width:60%
}
.second_modal_dialog {
	width:40%
}
.clearfix:after {
    content: "";
    display: table;
    clear: both;
}
</style>
</head>
<sec:authentication property="principal" var="principal" />
<div class="divContainer" ng-app="adminBookView" ng-controller="AdminBookCtrl">
	<div class="contentContainer">
		<form class="search_form">
    		<div class="form-group">
      			<div class="input-group">
        			<div class="input-group-addon"><i class="fa fa-search"></i></div>
        			<input type="text" class="form-control" placeholder="Search Books" ng-model="searchBook">
   				</div>      
   			</div>
  		</form>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<td class="tableCell titleTD">
						<a href="#"
							ng-click="sortType = 'bookTitle'; sortReverse = !sortReverse">Title:
								<span ng-show="sortType == 'bookTitle' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'bookTitle' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell authorTD">
						<a href="#"
							ng-click="sortType = 'bookAuthor'; sortReverse = !sortReverse">Author:
								<span ng-show="sortType == 'bookAuthor' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'bookAuthor' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell genreTD">
						<a href="#"
							ng-click="sortType = 'bookGenre'; sortReverse = !sortReverse">Genre:
								<span ng-show="sortType == 'bookGenre' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'bookGenre' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell priceTD">
						<a href="#"
							ng-click="sortType = 'bookPrice'; sortReverse = !sortReverse">Price:
								<span ng-show="sortType == 'bookPrice' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'bookPrice' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell conditionTD">
						<a href="#"
							ng-click="sortType = 'bookCondition'; sortReverse = !sortReverse">Quantity:
								<span ng-show="sortType == 'bookCondition' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'bookCondition' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell updateBookColumn" align="center" width="137px">Update Book</td>
					<!--<td class="tableCell deleteBookColumn" align="center" width="137px">Delete Book</td>-->
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="book in allBooks | orderBy:sortType:sortReverse | filter:searchBook | startFrom:currentPage*pageSize | limitTo:pageSize">
			    	<td>{{ book.bookTitle }}</td>
			    	<td>{{ book.bookAuthor }}</td>
			        <td>{{ book.bookGenre }}</td>
			        <td>&#8364;{{ book.bookPrice.toFixed(2) }}</td>
			        <td>{{ book.bookQuantity }}</td>
			        <td align="center" class="btn_cell">
			        	<button ng-click="updateBook(book)" data-toggle="modal" data-target="#myModal" class="btn btn-warning">Update</button>
		        	</td>
			       <!-- <td align="center" class="btn_cell">
			        	<button ng-click="deleteBook(book)"  data-toggle="modal" data-target="#myModal2" class="btn btn-danger">Delete</button>
			        </td>-->
				</tr>
			</tbody>
		</table>
		<div ng-show="selected">Selection from a modal: {{ selected }}</div>
		<div class="overflowPageDiv">
			<button class="switchPageBtn" ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1">Previous</button>
	    		{{currentPage+1}}/{{numberOfPages()}}
	    	<button class="switchPageBtn" ng-disabled="currentPage >= data.length/pageSize - 1" ng-click="currentPage=currentPage+1">Next</button>
		</div>
	</div>
	<div id="myModal" class="modal fade">
	    <div class="first_modal_dialog modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Edit Book</h4>
	            </div>
	            <div class="modal-body clearfix">
	                <div class="modalPictureDiv">
		    			<img src="${pageContext.request.contextPath}/static/images/{{activeBookToUpdate.bookImageTitle}}" width="100%" height="100%">
					</div>
					<form name='form' ng-submit="onSubmit()" novalidate="novalidate">
						<div class="modalEditableDiv">
		                	<div class="split-form-container">
								<div class="first-split-form">
									<div class="form-group form-group_2">
										<label for="id" class="control-label">Book ID:</label> <input
											type="text" class="form-control form-control_2"
											ng-model="activeBookToUpdate.id" id="id" disabled
											required="required">
									</div>
									<div class="form-group form-group_2">
										<label for="bookTitle" class="control-label">Title:</label> <input
											type="text" name="" class="form-control form-control_2" ng-disabled="formFieldsActive"
											ng-model="activeBookToUpdate.bookTitle" id="bookTitle" required="required">
									</div>
									<div class="form-group form-group_2">
										<label for="bookAuthor" class="control-label">Author:</label> <input
											type="text" class="form-control form-control_2" ng-disabled="formFieldsActive"
											ng-model="activeBookToUpdate.bookAuthor" id="bookAuthor" required="required">
									</div>
								</div>
								<div class="second-split-form">
									<div class="form-group form-group_2">
										<label for="bookPrice" class="control-label">Price &#8364;:
											</label> <input type="text" class="form-control form-control_2"
											ng-model="activeBookToUpdate.bookPrice" id="bookPrice" ng-disabled="formFieldsActive"
											required="required">
									</div>
									<div class="form-group form-group_2">
										<label for="bookGenre" class="control-label">Genre:
											</label> <input type="text" class="form-control form-control_2"
											ng-model="activeBookToUpdate.bookGenre" id="bookGenre"
											required="required" ng-disabled="formFieldsActive">
									</div>
									<div class="form-group form-group_2">
										<label for="bookQuantity" class="control-label">No. In Stock:
											</label> <input type="text" class="form-control form-control_2"
											ng-model="activeBookToUpdate.bookQuantity" id="bookQuantity"
											required="required" ng-disabled="formFieldsActive">
									</div>
								</div>
							</div>
		                </div>
		                	<label>Confirm?:
   							<input type="checkbox" ng-model="checkboxModel.value1">  
            			</form>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <button type="button" class="btn btn-primary" ng-disabled="!form.$dirty" ng-click="saveChanges()">Save changes</button>
	            </div>
	        </div>
	    </div>
	</div>
	<div id="myModal2" class="modal fade">
	    <div class="second_modal_dialog modal-dialog modal-sm">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Delete Book</h4>
	            </div>
	            <div class="modal-body clearfix">
	                <p>Are you sure you want to delete this book?</p>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <button type="button" class="btn btn-primary" ng-click="confirmedDelete()" data-dismiss="modal">Yes</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>